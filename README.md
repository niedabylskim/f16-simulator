# F16 Simulator

My master's thesis project. 
Realistic physics simulation of the F16 aircraft equipped with homing air to air missiles made using Unity Engine. Not only physics for the aircraft and missiles was developed but also a multiplayer mode which lets users play together. Aerodynamic data for the simulation as well as the mathematical model of aerodynamic coefficients of the F16 were obtained from NASA technical paper which can be found [here](https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19800005879.pdf). When it comes to homing air to air missiles five different algorithms of navigation were implemented starting from the simplest one finishing on the algorithms using Kalman Filtering and Augmented Proportional Navigation. Multiplayer mode was implemented using client-side prediction and server reconcilation. Server was also responsible for intergrating physics between players. Video presenting the project can be found on my youtube channel [here](https://www.youtube.com/channel/UCwFIHz3tGBjiFhNTuw2sMNQ)

![alt tag](./Pictures/gamePlay.png)

screen from the video


![alt tag](./Pictures/missileTrajectoriesComparison.png)

in the picture we can see different trajectories of missiles guided with different algorithms. This is a special case showing that some of the guidace algorithms can be tricked by an experienced pilot ;)

#Source
source code can be found in UnityProject/Assets/Scripts/