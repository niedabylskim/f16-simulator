﻿using Assets.Scripts.Physics;
using UnityEngine;

public static class MathHelper
{
    public const float G = 9.81f * ScaleDownFactor; // gravity, km/s^2

    public const float ScaleDownFactor = 0.1f;
    public const float ScaleDownFactorSquared = ScaleDownFactor * ScaleDownFactor;
    public const float ScaleDownFactorCubic = ScaleDownFactor * ScaleDownFactorSquared;
    public const float AngleErrorEpsilon = 5;
    public static Quaternion MultiplyFloat(this Quaternion a, float f)
    {
        return new Quaternion(a.x * f, a.y * f, a.z * f, a.w * f);
    }
    public static Quaternion AddQuaternion(this Quaternion a, Quaternion b)
    {
        return new Quaternion(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
    }
    public static Quaternion Normalize(this Quaternion a)
    {
        var len = Mathf.Sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
        return new Quaternion(a.x / len, a.y / len, a.z / len, a.w / len);
    }
    public static float GetLength(this Quaternion a)
    {
        return Mathf.Sqrt(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
    }

    private static float[] GetAtmosphericalConditions(float altitude, float speed)
    {
        return IsaAthmosphericModel.GetAtmosphericalConditions(altitude, speed);
    }
    public static float GetInterCoef(float value, float max, float clamp)
    {
        return Mathf.Clamp((max - Mathf.Abs(value)) / max, 0, clamp);
    }

    public static Vector3 GetLocalVelocity(float speed, float alpha, float beta)
    {
        float cosAlpha = Mathf.Cos(alpha);
        float cosBeta = Mathf.Cos(beta);
        float sinAlpha = Mathf.Sin(alpha);
        float sinBeta = Mathf.Sin(beta);
        return new Vector3
            (
                speed * cosAlpha * cosBeta,
                speed * sinBeta,
                speed * sinAlpha * cosBeta
            );
    }
    public static float CalculateLeadingEdgeFlap(float speed, float height, float area, float alfaAngle, out float[] atmosphereConditions)
    {
        atmosphereConditions = GetAtmosphericalConditions(height, speed);
        var QBar = atmosphereConditions[0];
        var Ps = atmosphereConditions[1];
        float lef = 1.38f * (2 * area + 7.25f) / (area + 7.25f) * alfaAngle - 9.05f * QBar / Ps + 1.45f;
        return Mathf.Clamp(lef, 0, 25);
    }

    public static Vector3 CoordinatesAircraftToUnity(Vector3 pos)
    {
        return new Vector3(pos.x, -pos.z, pos.y);
    }

    public static Vector3 GetPositionOnSphere(float r)
    {
        float alpha = Random.Range(0, 360);
        float beta = Random.Range(0, 360);
        float z = r * Mathf.Cos(alpha) * Mathf.Cos(beta);
        float y = r * Mathf.Sin(alpha) * Mathf.Cos(beta);
        float x = r * Mathf.Sin(beta);
        return new Vector3(x, y, z);
    }

    public static Vector3 GetPositionOnSphere(float alpha, float beta, float r)
    {
        float x = r * Mathf.Cos(alpha) * Mathf.Cos(beta);
        float y = r * Mathf.Sin(alpha) * Mathf.Cos(beta);
        float z = r * Mathf.Sin(beta);
        return new Vector3(x, y, z);
    }
    public static float GetRandomAngle(float minAngle, float maxAngle)
    {
        return Random.Range(minAngle, maxAngle);
    }
}
