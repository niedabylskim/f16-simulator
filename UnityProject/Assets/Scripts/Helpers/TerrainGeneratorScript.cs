﻿using UnityEngine;

public class TerrainGeneratorScript : MonoBehaviour {

    public GameObject[] terrs;
    float scale = 2;
    float size = 3500;
    int elemsCount = 2000;
	// Use this for initialization
	void Start () {
        float y = 0;
        for (int i = 0; i < elemsCount; i++)
        {
            int tNum = Random.Range(0, terrs.Length);
            float x = Random.Range(-size, size);
            float z = Random.Range(-size, size);
            float RotY = Random.value * 360;
            Quaternion q = Quaternion.Euler(0, RotY, 0);
            var inst = Instantiate(terrs[tNum], new Vector3(x, y, z), q);
            inst.transform.localScale = new Vector3(scale,scale,scale);
            inst.transform.SetParent(transform);
        }
	}
}
