﻿using Assets.Scripts.Aircraft;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public GameObject target;
    AircraftClient script;

    [Range(0, 100f)]
    public float distance = 15f;

    // Use this for initialization
    void Start()
    {
        transform.parent = null;
        script = target.GetComponent<AircraftClient>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!target)
        {
            Destroy(this);
            return;
        }
        Vector3 moveCamTo = target.transform.position - target.transform.right * distance + target.transform.up * 5.0f * MathHelper.ScaleDownFactor;
        float bias = 0.6f;
        transform.position = transform.position * bias + moveCamTo * (1.0f - bias);
        transform.LookAt(target.transform.position + target.transform.right * distance, target.transform.up);
    }
}
