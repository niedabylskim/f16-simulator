﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraController : NetworkBehaviour {

    // Use this for initialization
    private int indexOfCamera = 0;
    private List<Camera> cameras = new List<Camera>();
	void Start ()
    {
        cameras.AddRange(GetComponentsInChildren<Camera>());
        if (!isLocalPlayer)
        {
            foreach (var cam in cameras) cam.gameObject.SetActive(false);
            return;
        }
        indexOfCamera = cameras.FindIndex(c => c.tag == "MainCamera");
        foreach (var cam in cameras.FindAll(c=>c.tag!="MainCamera"))
            cam.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!isLocalPlayer) return;
        if (Input.GetButtonDown("BButton"))
        {
            cameras[indexOfCamera++].gameObject.SetActive(false);
            if (indexOfCamera >= cameras.Count) indexOfCamera = 0;
            cameras[indexOfCamera].gameObject.SetActive(true);
            if (cameras[indexOfCamera].gameObject.name == "CameraCockpit")
            {
                transform.localScale = new Vector3(5, 5, 5);
                ChangeAudioSourceMinDistance(30);
            }
            else
            {
                transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                ChangeAudioSourceMinDistance(1);
            }
        }
    }
    private void ChangeAudioSourceMinDistance(float dist)
    {
        var audioSource = GetComponent<AudioSource>();
        audioSource.minDistance = dist;
    }
}
