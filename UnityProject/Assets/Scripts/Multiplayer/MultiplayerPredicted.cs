﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class MultiplayerPredicted : MonoBehaviour, IClientStateHandler
    {
        private List<Input> pendingInputs;
        private MultiplayerClient player;
        private State predictedState;

        void Awake()
        {
            pendingInputs = new List<Input>();
            player = GetComponent<MultiplayerClient>();
            UpdatePredictedState();
        }

        public void OnStateChanged(State newState)
        {
            if (pendingInputs == null) return;
            predictedState = newState;
            int i = 0;
            while (i < pendingInputs.Count)
            {
                if (pendingInputs[i].Info.MoveNumber <= predictedState.Info.MoveNumber)
                    pendingInputs.RemoveAt(i);
                else
                {
                    predictedState = State.Move(predictedState, pendingInputs[i]);
                    i++;
                }
            }
        }

        private void UpdatePredictedState()
        {
            predictedState = player.serverState;
            foreach (var input in pendingInputs)
                predictedState = State.Move(predictedState, input);
            //player.SyncState(predictedState);
        }
        

        public void AddInput(Input input)
        {
            pendingInputs.Add(input);
            predictedState = State.Move(predictedState, input);
            //player.SyncState(predictedState);
        }

        public State GetCurrentState()
        {
            return predictedState;
        }
    }
}
