﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class MultiplayerServer : MonoBehaviour
    {
        private Queue<Input> bufferOnServer;
        private const double sendRate = 10;
        private double lastSyncTime;

        MultiplayerClient player;

        void Awake()
        {
            bufferOnServer = new Queue<Input>();
            player = GetComponent<MultiplayerClient>();
            player.serverState = State.CreateStartState();
            lastSyncTime = MultiplayerManager.GetTime();
        }

        void FixedUpdate()
        {
            UpdateServer();
        }

        void UpdateServer()
        {
            double tmpt = Network.time;
            if (tmpt - lastSyncTime > 1 / sendRate)
            {
                lastSyncTime = tmpt;
                MoveAutoritative();
            }
        }

        private void MoveAutoritative()
        {
            State tmp = player.serverState;
            while (bufferOnServer.Count != 0)
            {
                var q = bufferOnServer.Dequeue();
                tmp = new State(q.Info, tmp.Position + q.dPosition * tmp.Speed);
            }
            player.serverState = tmp;
        }

        public void AddInput(Input input)
        {
            bufferOnServer.Enqueue(input);
        }
    }
}