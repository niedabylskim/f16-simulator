﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


namespace Assets.Scripts.Multiplayer
{
    public class MultiplayerManager : NetworkManager
    {
        private static double time;
        private static double deltaTime;
        public static bool isTimeSynced = false;
        private const short SyncTimeMessageId = MsgType.Highest + 1;

        public static int rtt;

        public bool IsConnecting { get; private set; }

        public static double GetTime()
        {
            if (Network.isServer) time = Network.time;
            else time = Network.time + deltaTime;

            //if (Network.isServer) Debug.Log("SERVERTIME: " + time);
            //else Debug.Log("CLIENTTIME: " + time);
            return time;
        }
        public static double GetRTT()
        {
            return singleton.client.GetRTT();
        }
        public override void OnStartServer()
        {
            base.OnStartServer();
            time = Network.time;
            deltaTime = 0;
            isTimeSynced = true;
            Debug.Log("onstartserver");
        }

        public override void OnStopServer()
        {
            base.OnStopServer();
            isTimeSynced = false;
            Debug.Log("onstopserver");
        }

        //Called on server when client connects
        public override void OnServerConnect(NetworkConnection conn)
        {
            base.OnServerConnect(conn);
            SyncTimeMessage mes = new SyncTimeMessage { ServerTime = (float)Network.time };
            conn.Send(SyncTimeMessageId, mes);
            //TargetSetDeltaSync(conn, (float)Network.time);
            Debug.Log("onserverconnect");
        }

        public override void OnStopClient()
        {
            base.OnStopClient();
            isTimeSynced = false;
        }

        //Called on client
        public override void OnStartClient(NetworkClient client)
        {
            base.OnStartClient(client);
            client.RegisterHandler(SyncTimeMessageId, OnReceiveSyncTime);
            Debug.Log("onstartclient: " + client.isConnected);  
        }

        public override void OnClientConnect(NetworkConnection conn)
        {
            base.OnClientConnect(conn);
            SettingsManager.Instance.StopPlayingSong();
            SettingsManager.Instance.ChangeIsConnecting(false);
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            SettingsManager.Instance.ChangeIsConnecting(false);
        }

        private void OnReceiveSyncTime(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<SyncTimeMessage>();
            if (isTimeSynced) return;
            rtt = singleton.client.GetRTT();
            deltaTime = msg.ServerTime - Network.time;
            isTimeSynced = true;
            Debug.Log("Delta from network.time to server's network.time is " + deltaTime);
        }

        public override void OnServerDisconnect(NetworkConnection conn)
        {
            if (conn.lastError != NetworkError.Ok && conn.lastError != NetworkError.Timeout)
            {
                Debug.LogError("Something went wrong");
                return;
            }
            NetworkServer.DestroyPlayersForConnection(conn);
            conn.Disconnect();
            Debug.Log("OnServerDisconnect");
        }
    }

    public class SyncTimeMessage : MessageBase
    {
        public double ServerTime;
    }
}
