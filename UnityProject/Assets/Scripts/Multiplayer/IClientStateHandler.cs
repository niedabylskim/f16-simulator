﻿namespace Assets.Scripts.Multiplayer
{
    public interface IClientStateHandler
    {
        void OnStateChanged(State newState);

        State GetCurrentState();
    }
}