﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public struct Input
    {
        public Vector2 dPosition;
        public MoveInfo Info;

        public Input(MoveInfo mi, float dx = 0, float dy = 0)
        {
            dPosition = new Vector2(dx, dy);
            Info = mi;
        }
    }
}
