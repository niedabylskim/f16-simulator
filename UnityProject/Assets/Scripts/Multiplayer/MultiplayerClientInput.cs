﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class MultiplayerClientInput : MonoBehaviour
    {
        List<Input> inputBuffer;
        MultiplayerClient player;
        MultiplayerPredicted predicted;

        void Awake()
        {
            inputBuffer = new List<Input>();
            player = GetComponent<MultiplayerClient>();
            predicted = GetComponent<MultiplayerPredicted>();
        }

        void FixedUpdate()
        {
            ProcessInput();
        }

        public void ProcessInput()
        {
            float x = UnityEngine.Input.GetAxis("Horizontal");
            float y = UnityEngine.Input.GetAxis("Vertical");
            if (x != 0 || y != 0)
            {
                Input input = new Input(new MoveInfo(player.GetMachineTime()), x, y);
                predicted.AddInput(input);
                player.CmdMoveOnServer(input);
            }

        }
    }
}
