﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Multiplayer
{
    public struct MoveInfo
    {
        public uint MoveNumber;
        public double Timestamp;
        private static uint count = 0;

        public MoveInfo(double timestamp = 0)
        {
            MoveNumber = count++;
            Timestamp = timestamp;
        }
    }
}
