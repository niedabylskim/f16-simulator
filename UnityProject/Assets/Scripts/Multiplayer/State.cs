﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public struct State
    {
        public Vector2 Position;
        public float Speed;
        public MoveInfo Info;
        static int count = 0;
        public int ID;

        public State(MoveInfo mi, float x = 5, float y = 0, float speed = 1)
        {
            ID = count++;
            Position = new Vector2(x, y);
            Speed = speed;
            Info = mi;
        }

        public State(MoveInfo mi, Vector2 pos, float speed = 1)
        {
            ID = count++;
            Position = pos;
            Speed = speed;
            Info = mi;
        }

        public static State CreateStartState()
        {
            return new State(new MoveInfo(), 5, 0);
        }

        public static State Move(State state, Input input)
        {
            return new State(input.Info, state.Position + input.dPosition * state.Speed);
        }

        public override string ToString()
        {
            return ID + " State pos: " + Position.ToString();
        }
    }
}
