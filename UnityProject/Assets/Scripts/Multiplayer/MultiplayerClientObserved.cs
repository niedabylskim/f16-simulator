﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class MultiplayerClientObserved : MonoBehaviour, IClientStateHandler
    {
        State interpolated;
        MultiplayerClient player;
        LinkedList<State> interpolationBuffer;
        private float interpolationCoeff = 0.6f;
        void Awake()
        {
            player = GetComponent<MultiplayerClient>();
            interpolationBuffer = new LinkedList<State>();
            SetObservedState(player.serverState);
            AddState(player.serverState);
        }

        void FixedUpdate()
        {
            InterpolateEntities();
        }

        public void OnStateChanged(State newState)
        {
            AddState(newState);
        }
        void SetObservedState(State newState)
        {
            interpolated = newState;
            //player.SyncState(interpolated);
        }

        void AddState(State state)
        {
            interpolationBuffer.AddLast(state);
        }

        private void InterpolateEntities()
        {
            double shift = (0.3);
            //if (shift < 1) shift = 1;
            var renderTimestamp = player.GetMachineTime() - shift;
            while (interpolationBuffer.Count >= 2 && interpolationBuffer.First.Next.Value.Info.Timestamp <= renderTimestamp)
            {
                interpolationBuffer.RemoveFirst();
            }
            if (interpolationBuffer.Count >= 2
                && interpolationBuffer.First.Value.Info.Timestamp <= renderTimestamp
                && renderTimestamp <= interpolationBuffer.First.Next.Value.Info.Timestamp)
            {
                var t0 = interpolationBuffer.First.Value.Info.Timestamp;
                var t1 = interpolationBuffer.First.Next.Value.Info.Timestamp;
                var x0 = interpolationBuffer.First.Value.Position;
                var x1 = interpolationBuffer.First.Next.Value.Position;
                float coeff = (float)((renderTimestamp - t0) / (t1 - t0));
                //Debug.Log(coeff);
                //clientState.Position = x0 + (x1 - x0) * coeff;
                interpolated.Position = Vector2.Lerp(x0, x1, coeff);
            }
            if (interpolationBuffer.Count == 1 && interpolationBuffer.First.Value.Info.Timestamp <= renderTimestamp)
            {
                //clientState.Position = Vector2.Lerp(clientState.Position, interpolationBuffer.First.Value.Position, interpolationCoeff);
                interpolated.Position = interpolationBuffer.First.Value.Position;
                //interpolationBuffer.RemoveFirst();
            }
            SetObservedState(interpolated);
        }

        public State GetCurrentState()
        {
            return interpolated;
        }
    }
}
