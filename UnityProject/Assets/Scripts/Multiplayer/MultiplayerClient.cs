﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Assets.Scripts.Multiplayer
{
    public class MultiplayerClient : NetworkBehaviour
    {
        [SyncVar(hook = "OnServerUpdate")]
        public State serverState;

        MultiplayerServer server;
        IClientStateHandler stateHandler;

        Text textTime;
        void Awake()
        {
            AwakeOnServer();
        }
        void Start()
        {
            Debug.Log(Network.sendRate);
            if (!isLocalPlayer)
            {
                stateHandler = gameObject.AddComponent<MultiplayerClientObserved>();
                return;
            }
            stateHandler = gameObject.AddComponent<MultiplayerPredicted>();
            gameObject.AddComponent<MultiplayerClientInput>();
            textTime = GetComponentInChildren<Text>();
        }
        void FixedUpdate()
        {
            if(isLocalPlayer) textTime.text = "rtt: " + MultiplayerManager.GetRTT() + "\n" + MultiplayerManager.GetTime().ToString("F3");
            transform.position = stateHandler.GetCurrentState().Position;
        }

        [Server]
        void AwakeOnServer()
        {
            server = gameObject.AddComponent<MultiplayerServer>();
        }

        void OnServerUpdate(State newState)
        {
            serverState = newState;
            if (stateHandler == null) return;
            stateHandler.OnStateChanged(serverState);
        }

        public double GetMachineTime()
        {
            return MultiplayerManager.GetTime();
        }


        [Command]
        public void CmdMoveOnServer(Input input)
        {
            server.AddInput(input);
        }
    }
}
