﻿using System;
using UnityEngine;

namespace Assets.Scripts.Physics
{
    public static class PhysicsSolver
    {
        private static float h = 0.01f;

        public delegate bool GetStateDerivatives(RigidBody r, Parameters s, out State result);

        public static bool SolveEquationsOfMotion(RigidBody r, Parameters p0, out State result)
        {
            return SolverRK4(r, p0, GetDerivatives, out result);
        }
        
        private static bool GetDerivatives(RigidBody r, Parameters p, out State result)
        {
            if (p.St.Speed < 0.005f)
                p.St = new State { Position = p.St.Position, Speed = 0.05f, Alpha = p.St.Alpha, Beta = p.St.Beta, Orientation = p.St.Orientation, AngularVeloctiy = p.St.AngularVeloctiy };

            Vector3 F, M;
            
            if (!r.GetForcesAndMoments(p, out F, out M))
            {
                result = p.St;
                return false;
            }
            var q = p.St.Orientation;
            var v = MathHelper.GetLocalVelocity(p.St.Speed, p.St.Alpha, p.St.Beta);
            var w = p.St.AngularVeloctiy;
            var wq = new Quaternion(w.x, w.y, w.z, 0);

            var xdot = q * v;
            var qdot = (q * wq).MultiplyFloat(0.5f);
            var vdot = F / r.M - Vector3.Cross(w, v);
            var wdot = r.Iinv * (M - Vector3.Cross(w, r.I * w));

            var velDot = Vector3.Dot(v, vdot) / p.St.Speed;
            var alphaDot = (v.x * vdot.z - v.z * vdot.x) / (v.x * v.x + v.z * v.z); // vector3 angle???
            var betaDot = (p.St.Speed * vdot.y - v.y * velDot) / (p.St.Speed * p.St.Speed * Mathf.Cos(p.St.Beta)); // vector3 angle???

            result =  new State { Position = xdot, Speed = velDot, Alpha = alphaDot, Beta = betaDot, Orientation = qdot, AngularVeloctiy = wdot } * h;
            return true;
        }
        
        public static bool SolverRK4(RigidBody r, Parameters p0, GetStateDerivatives F, out State result)
        {
            State k1, k2, k3, k4;
            if (
                F(r, p0, out k1) &&
                F(r, p0 + k1 * 0.5f, out k2) &&
                F(r, p0 + k2 * 0.5f, out k3) &&
                F(r, p0 + k3, out k4)
                )
            {
                result = p0.St + (k1 + k2 * 2.0f + k3 * 2.0f + k4) / 6.0f;
                result.Orientation = result.Orientation.Normalize();
                return true;
            }
            result = r.St;
            return false;
        }

    }
}
