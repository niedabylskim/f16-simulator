﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Assets.Scripts.Physics
{
    public abstract class RigidBody
    {
        public State St { get; set; }
        public float M { get; protected set; }
        public Matrix4x4 I { get; protected set; }
        public Matrix4x4 Iinv { get; protected set; }
        public RigidBody()
        {
            St = new State
            {
                Speed = 0,
                Alpha = 0,
                Beta = 0,
                Position = new Vector3(0, 0, 0),
                AngularVeloctiy = Vector3.zero,
                Orientation = Quaternion.identity
            };
        }
        public RigidBody(Vector3 translation, Quaternion rotation, float velocity = 0, Vector3? angularVelocity = null)
        {
            St = new State
            {
                Speed = velocity,
                Alpha = 0,
                Beta = 0,
                Position = translation,
                AngularVeloctiy = angularVelocity == null ? Vector3.zero : (Vector3)angularVelocity,
                Orientation = rotation
            };
        }

        public abstract bool GetForcesAndMoments(Parameters p, out Vector3 forces, out Vector3 moments);
    }
}