﻿namespace Assets.Scripts.Physics
{
    public abstract class Parameters
    {
        public State St { get; set; }

        public static Parameters operator +(Parameters a, State b)
        {
            return a.Add(b);
        }
        public static Parameters operator *(Parameters a, float f)
        {
            return a.Multiply(f);
        }
        public static Parameters operator /(Parameters a, float f)
        {
            return a.Divide(f);
        }

        protected abstract Parameters Add(State b);
        protected abstract Parameters Multiply(float f);
        protected abstract Parameters Divide(float f);
    }
}