﻿using Assets.Scripts.Aircraft;
using Assets.Scripts.Aircraft;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Assets.Scripts.Physics
{
    public struct State
    {
        public Vector3 Position;
        public float Speed;
        public float Alpha;
        public float Beta;
        public Vector3 AngularVeloctiy;
        public Quaternion Orientation;
        public static State operator +(State a, State b)
        {
            return new State
            {
                Position = a.Position + b.Position,
                Speed = a.Speed + b.Speed,
                Alpha = a.Alpha + b.Alpha,
                Beta = a.Beta + b.Beta,
                AngularVeloctiy = a.AngularVeloctiy + b.AngularVeloctiy,
                Orientation = a.Orientation.AddQuaternion(b.Orientation)
            };
        }
        public static State operator *(State a, float f)
        {
            return new State
            {
                Position = a.Position * f,
                Speed = a.Speed * f,
                Alpha = a.Alpha * f,
                Beta = a.Beta * f,
                AngularVeloctiy = a.AngularVeloctiy * f,
                Orientation = a.Orientation.MultiplyFloat(f)
            };
        }
        public static State operator /(State a, float f)
        {
            return new State
            {
                Position = a.Position / f,
                Speed = a.Speed / f,
                Alpha = a.Alpha / f,
                Beta = a.Beta / f,
                AngularVeloctiy = a.AngularVeloctiy / f,
                Orientation = a.Orientation.MultiplyFloat(1.0f / f)
            };
        }

        public MoveInfo Info;
        public static State CreateStartState()
        {
            return new State { Position = new Vector3(0, 0, -1000 * MathHelper.ScaleDownFactor), Orientation = Quaternion.identity, Speed = 350 * MathHelper.ScaleDownFactor, Info = new MoveInfo() };
        }

        public static bool Move(State state, AircraftControl input, Aircraft.Aircraft jet, out State result)
        {
            AircraftParameters p0 = new AircraftParameters { St = state, Control = input, Specification = jet.Specification };
            if (PhysicsSolver.SolveEquationsOfMotion(jet, p0, out result))
            {
                jet.Control = p0.Control;
                result.Info = input.Info;
                return true;
            }
            return false;
        }
        //public static State Move(State state, AircraftControl input, RigidBody r)
        //{
        //    Parameters p0 = new AircraftParameters { St = state };
        //    var nState = PhysicsSolver.SolveEquationsOfMotion(r, p0);
        //    nState.Info = input.Info;
        //    return nState;
        //}
    }

    public struct MoveInfo
    {
        public uint MoveNumber;
        public double Timestamp;
        private static uint count = 0;

        public MoveInfo(double timestamp = 0)
        {
            MoveNumber = count++;
            Timestamp = timestamp;
        }
    }

}

