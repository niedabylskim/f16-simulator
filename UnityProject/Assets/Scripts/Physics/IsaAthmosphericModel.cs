﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Assets.Scripts.Physics
{
    public static class IsaAthmosphericModel
    {
        /// <summary>
        /// gas constant for air in J / (Kg * K)
        /// </summary>
        private const float R = 289.6f;
        /// <summary>
        /// standard temperature at sea level in Kelvin degrees
        /// </summary>
        private const float t0 = 288.15f;
        /// <summary>
        /// Temerature lapse rate K / m
        /// </summary>
        private const float L = 0.0065f / MathHelper.ScaleDownFactor;
        /// <summary>
        /// standard air density at sea level in Kg / m^3
        /// </summary>
        private const float rh0 = 1.225f / (MathHelper.ScaleDownFactorCubic);
        /// <summary>
        /// maximum altitude at which atmopsheric model is correct
        /// </summary>
        private const float maxAltitude = 11000.0f * MathHelper.ScaleDownFactor;
        /// <summary>
        /// approximation of temperature above troposhere
        /// </summary>
        private const float tAboveTroposhere = 216.95f;
        /// <summary>
        /// tLoss / t0
        /// </summary>
        private const float tLossToT0Ratio = 2.255769e-5f;
        /// <summary>
        /// gM / (R*L) g = gravitational acceleration, R - ideal gas constant, M - molar mass, L - temperature lapse rate
        /// </summary>
        private const float power = 5.2561f;
        
        public static float[] GetAtmosphericalConditions(float altitude, float speed)
        {
            var temp = t0 - L * altitude;
            var tFacNorm = 1 - tLossToT0Ratio * altitude;
            var pow = Mathf.Pow(tFacNorm, power);
            var rh = rh0 * pow;
            if (altitude >= maxAltitude) temp = tAboveTroposhere;
            var ps = R * rh * temp;
            var qbar = 0.5f * rh * speed * speed; // dynamic pressure
            return new float[] { qbar, ps };
        }

    }
}