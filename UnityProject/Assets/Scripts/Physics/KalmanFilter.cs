﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using UnityEngine;

namespace Assets.Scripts.Physics
{
    public class KalmanFilter
    {
        Vector<double> X;
        Matrix<double> A;
        Matrix<double> Q;
        Matrix<double> H;
        Matrix<double> R;
        Matrix<double> P;
        
        public KalmanFilter(Vector3 position, Vector3 velocity, float dt)
        {
            Vector<double> input = Vector<double>.Build.DenseOfArray(new double[] { position.x, position.y, position.z,
                                                                                velocity.x, velocity.y, velocity.z });
            Initialize(input, dt);
        }
        private void Initialize(Vector<double> x, double dt)
        {
            X = x;
            A = DenseMatrix.OfArray(new double[,] { {1,0,0,dt,0,0 },
                                                    {0,1,0,0,dt,0 },
                                                    {0,0,1,0,0,dt },
                                                    {0,0,0,1,0,0 },
                                                    {0,0,0,0,1,0 },
                                                    {0,0,0,0,0,1 } });
            Q = DenseMatrix.CreateDiagonal(6, 6, 0.01);
            R = DenseMatrix.CreateDiagonal(6, 6, 0.01);
            P = DenseMatrix.OfArray(new double[,] { {1,0,0,0,0,0 },
                                                    {0,1,0,0,0,0 },
                                                    {0,0,1,0,0,0 },
                                                    {0,0,0,1,0,0 },
                                                    {0,0,0,0,1,0 },
                                                    {0,0,0,0,0,1 } });
            H = DenseMatrix.OfArray(new double[,] { {1,0,0,0,0,0 },
                                                    {0,1,0,0,0,0 },
                                                    {0,0,1,0,0,0 },
                                                    {0,0,0,1,0,0 },
                                                    {0,0,0,0,1,0 },
                                                    {0,0,0,0,0,1 } });
        }
        private Vector<double> Predict(Vector<double> Z, double dt)
        {
            A = DenseMatrix.OfArray(new double[,] { {1,0,0,dt,0,0 },
                                                    {0,1,0,0,dt,0 },
                                                    {0,0,1,0,0,dt },
                                                    {0,0,0,1,0,0 },
                                                    {0,0,0,0,1,0 },
                                                    {0,0,0,0,0,1 } });

            X = A * X;
            P = A * P * A + Q;
            var K = P * H * (H * P * H + R).Inverse();
            X = X + K * (Z - H * X);
            P = P - K * H * P;

            return X;
        }

        public Vector3 Predict(Vector3 position, Vector3 velocity, double dt)
        {
            Vector<double> input = Vector<double>.Build.DenseOfArray(new double[] { position.x, position.y, position.z,
                                                                                velocity.x, velocity.y, velocity.z });
            var result = Predict(input, dt);
            return new Vector3((float)result[0], (float)result[1], (float)result[2]);
        }
    }
}
