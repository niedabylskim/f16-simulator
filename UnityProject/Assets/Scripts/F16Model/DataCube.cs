﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

[DataContract]
public class DataCube
{
    [DataMember(Name = "SizeX")]
    private int sizeX;
    [DataMember(Name = "SizeY")]
    private int sizeY;
    [DataMember(Name = "SizeZ")]
    private int sizeZ;
    [DataMember(Name = "Scales")]
    private List<int[]> scales;
    [DataMember(Name = "Data")]
    private float[] data;

    public DataCube() { }
    public float this[int i, int j = 0, int k = 0]
    {
        get
        {
            return data[sizeX * sizeY * k + j * sizeX + i];
        }
    }

    /// <summary>
    /// Returns closest(smaller) index to value in array.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="array"></param>
    /// <returns>Closest (smaller) index or -1 if value was smaller than lower bound.</returns>
    private int GetIndex(float value, int[] array)
    {
        int i = Array.BinarySearch(array, (int)value);
        if (i == array.Length - 1) i--;
        if (i < 0) i = (~i) - 1;
        if (i < 0) return -1;
        if (i >= array.Length - 1) i = array.Length - 2;
        return i;
    }

    public bool Get(float x, float y, float z, out float result)
    {
        int i = GetIndex(x, scales[0]);
        int j = GetIndex(y, scales[1]);
        int k = GetIndex(z, scales[2]);
        result = 0;
        if (i == -1 || j == -1 || k == -1)
        {
            return false;
        }
        float x0 = scales[0][i];
        float y0 = scales[1][j];
        float z0 = scales[2][k];
        float x1 = scales[0][i + 1];
        float y1 = scales[1][j + 1];
        float z1 = scales[2][k + 1];

        float xd = (x - x0) / (x1 - x0);
        float yd = (y - y0) / (y1 - y0);
        float zd = (z - z0) / (z1 - z0);

        float c000 = this[i, j, k];
        float c100 = this[i + 1, j, k];
        float c001 = this[i, j, k + 1];
        float c101 = this[i + 1, j, k + 1];
        float c010 = this[i, j + 1, k];
        float c110 = this[i + 1, j + 1, k];
        float c011 = this[i, j + 1, k + 1];
        float c111 = this[i + 1, j + 1, k + 1];

        float c0 = BilinearInterpolation(yd, zd, c000, c010, c001, c011);
        float c1 = BilinearInterpolation(yd, zd, c100, c110, c101, c111);
        result = LinearInterpolation(xd, c0, c1);
        return true;
    }
    public bool Get(float x, float y, out float result)
    {
        int i = GetIndex(x, scales[0]);
        int j = GetIndex(y, scales[1]);
        result = 0;
        if (i == -1 || j == -1)
        {
            return false;
        }
        float x0 = scales[0][i];
        float y0 = scales[1][j];
        float x1 = scales[0][i + 1];
        float y1 = scales[1][j + 1];

        float xd = (x - x0) / (x1 - x0);
        float yd = (y - y0) / (y1 - y0);

        float c00 = this[i, j];
        float c10 = this[i + 1, j];
        float c01 = this[i, j + 1];
        float c11 = this[i + 1, j + 1];

        result = BilinearInterpolation(xd, yd, c00, c10, c01, c11);
        return true;
    }
    public bool Get(float x, out float result)
    {
        int i = GetIndex(x, scales[0]);
        result = 0;
        if (i == -1)
        {
            return false;
        }
        float x0 = scales[0][i];
        float x1 = scales[0][i + 1];

        float xd = (x - x0) / (x1 - x0);
        float c0 = this[i];
        float c1 = this[i + 1];

        result = LinearInterpolation(xd, c0, c1);
        return true;
    }
    private float LinearInterpolation(float xd, float c0, float c1)
    {
        return c0 * (1 - xd) + c1 * xd;
    }
    private float BilinearInterpolation(float xd, float yd, float c00, float c10, float c01, float c11)
    {
        float c0 = LinearInterpolation(yd, c00, c01);
        float c1 = LinearInterpolation(yd, c10, c11);
        return LinearInterpolation(xd, c0, c1);
    }
}