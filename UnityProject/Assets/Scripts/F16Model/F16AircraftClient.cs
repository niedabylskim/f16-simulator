﻿using Assets.Scripts.Aircraft;
using UnityEngine;

namespace Assets.Scripts.F16Model
{
    public class F16AircraftClient : AircraftClient
    {
        public PhysicalModelEnum Type;

        protected override void Awake()
        {
            base.Awake();
            Type = SettingsManager.Instance.IsHifiModel ? PhysicalModelEnum.Hifi : PhysicalModelEnum.Lofi;
            jet = new F16Aircraft(Type)
            {
                Control = new AircraftControl { Aileron = 0, Rudder = 0, Elevator = -1.2516f, Lef = 0, Thrust = 41204.1054646f * MathHelper.ScaleDownFactor, ThrustAngle = 0 }
            };
        }

        protected override void GetInfoText(out string positionText, out string controlText)
        {
            base.GetInfoText(out positionText, out controlText);

            positionText = "Model: " + Type.ToString() + "\n" + positionText;
            float[] atmoshpericConditions;
            float angleAlpha = Mathf.Rad2Deg * jet.St.Alpha;
            jet.Control.Lef = ((F16Aircraft)jet).GetPhysicalModel() == PhysicalModelEnum.Hifi
                ? MathHelper.CalculateLeadingEdgeFlap(jet.St.Speed, -jet.St.Position.z, jet.Specification.S, angleAlpha, out atmoshpericConditions)
                : 0;
            
            controlText += "Lef: " + jet.Control.Lef.ToString(format) + "\n";
        }
    }
}
