﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using UnityEngine;
using Assets.Scripts.Physics;
using Assets.Scripts.Aircraft;

namespace Assets.Scripts.F16Model
{
    public class HifiAerodynamicData : IAerodynamicData
    {
        private static Dictionary<string, DataCube> tables;
        private const string directory = @"Resources\F16FlightData\";
        static HifiAerodynamicData()
        {
            tables = new Dictionary<string, DataCube>();

            var files = Resources.LoadAll("F16FlightData\\");
            foreach (var f in files)
            {
                var t = f as TextAsset;
                tables.Add(t.name, ReadFromFile(t.text));
            }
        }
        private static DataCube ReadFromFile(string xmlContent)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(xmlContent)))
            {
                var xml = new DataContractSerializer(typeof(DataCube));
                return (DataCube)xml.ReadObject(reader);
            }
        }
        public bool GetAerodynamicForcesAndMoments(Parameters p, out Vector3 forces, out Vector3 moments)
        {
            var ap = new F16ForcesParameters(p as AircraftParameters);

            float x, y, z, l, m, n;

            forces = Vector3.zero;
            moments = Vector3.zero;

            if (GetXForce(ap, out x) &&
                GetYForce(ap, out y) &&
                GetZForce(ap, out z) &&
                GetLMoment(ap, out l) &&
                GetMMoment(ap, z, out m) &&
                GetNMoment(ap, y, out n)
                )
            {
                var qbarS = ap.QBar * ap.Specification.S;
                forces = new Vector3(x * qbarS, y * qbarS, z * qbarS);
                moments = new Vector3(l, m, n);
                return true;
            }

            return false;


        }
        private bool GetXForce(F16ForcesParameters ap, out float cxForce)
        {
            float q = ap.St.AngularVeloctiy.y;
            float cq2v = (ap.Specification.Cbar * q) / (2 * ap.St.Speed);
            var alpha = ap.AlphaDeg;
            var beta = ap.BetaDeg;

            float cxlef, cx0, cx, cxsb_delta, cxq, cxqlef_delta;
            cxForce = 0;

            if (tables["Cxlef"].Get(alpha, beta, out cxlef)
                && tables["Cx"].Get(alpha, beta, 0, out cx0)
                && tables["Cx"].Get(alpha, beta, ap.Control.Elevator, out cx)
                && tables["Cxsb_delta"].Get(alpha, out cxsb_delta)
                && tables["Cxq"].Get(alpha, out cxq)
                && tables["Cxqlef_delta"].Get(alpha, out cxqlef_delta))
            {
                var cxlef_delta = cxlef - cx0;
                cxForce = cx + cxlef_delta * ap.GLefRatioCompl + cxsb_delta * ap.GSbRatio
                    + cq2v * (cxq + cxqlef_delta * ap.GLefRatioCompl);
                return true;
            }
            else return false;
        }
        private bool GetYForce(F16ForcesParameters ap, out float cyForce)
        {
            var alpha = ap.AlphaDeg;
            var beta = ap.BetaDeg;
            float p = ap.St.AngularVeloctiy.x;
            float r = ap.St.AngularVeloctiy.z;
            float b2v = ap.Specification.B / (2 * ap.St.Speed);

            float cylef, cy, cyga, cygalef, cygr, cyr, cyp, cyplef_delta, cyrlef_delta;

            cyForce = 0;

            if (tables["Cylef"].Get(alpha, beta, out cylef) &&
                tables["Cy"].Get(alpha, beta, out cy) &&
                tables["Cyga"].Get(alpha, beta, out cyga) &&
                tables["Cygalef"].Get(alpha, beta, out cygalef) &&
                tables["Cygr"].Get(alpha, beta, out cygr) &&
                tables["Cyr"].Get(alpha, out cyr) &&
                tables["Cyrlef_delta"].Get(alpha, out cyrlef_delta) &&
                tables["Cyp"].Get(alpha, out cyp) &&
                tables["Cyplef_delta"].Get(alpha, out cyplef_delta)
                )
            {
                var cylef_delta = cylef - cy;
                var cyga_delta = cyga - cy;
                var cygalef_delta = cygalef - cylef - cyga_delta;
                var cygr_delta = cygr - cy;

                cyForce = cy + cylef_delta * ap.GLefRatioCompl
                    + (cyga_delta + cygalef_delta * ap.GLefRatioCompl) * ap.GAilRatio
                    + cygr_delta * ap.GRudRatio + b2v * ((cyr + cyrlef_delta * ap.GLefRatioCompl) * r
                    + (cyp + cyplef_delta * ap.GLefRatioCompl) * p);
                return true;
            }
            else return false;
        }
        private bool GetZForce(F16ForcesParameters ap, out float czForce)
        {
            var alpha = ap.AlphaDeg;
            var beta = ap.BetaDeg;
            float q = ap.St.AngularVeloctiy.y;
            float cq2v = (ap.Specification.Cbar * q) / (2 * ap.St.Speed);

            czForce = 0;
            float czlef, cz0, cz, czsb_delta, czq, czqlef_delta;
            if (
                tables["Czlef"].Get(alpha, beta, out czlef) &&
                tables["Cz"].Get(alpha, beta, 0, out cz0) &&
                tables["Cz"].Get(alpha, beta, ap.Control.Elevator, out cz) &&
                 tables["Czsb_delta"].Get(alpha, out czsb_delta) &&
                 tables["Czq"].Get(alpha, out czq) &&
                 tables["Czqlef_delta"].Get(alpha, out czqlef_delta)
                )
            {
                var czlef_delta = czlef - cz0;
                czForce = cz + czlef_delta * ap.GLefRatioCompl + czsb_delta * ap.GSbRatio
                    + cq2v * (czq + czqlef_delta * ap.GLefRatioCompl);
                return true;
            }
            else return false;
        }
        private bool GetMMoment(F16ForcesParameters ap, float cz, out float cmMoment)
        {
            var alpha = ap.AlphaDeg;
            var beta = ap.BetaDeg;
            float q = ap.St.AngularVeloctiy.y;
            float cq2v = (ap.Specification.Cbar * q) / (2 * ap.St.Speed);
            float xc = ap.Specification.Xcgr - ap.Specification.Xcg;

            cmMoment = 0;

            float cmlef, cm0, cm, gh_psi, cmsb_delta, cmq, cmqlef_delta, cm_delta, cmds_delta;

            if (tables["Cmlef"].Get(alpha, beta, out cmlef) &&
                tables["Cm"].Get(alpha, beta, 0, out cm0) &&
                tables["Cm"].Get(alpha, beta, ap.Control.Elevator, out cm) &&
                tables["Gh_psi"].Get(ap.Control.Elevator, out gh_psi) &&
                tables["Cmsb_delta"].Get(alpha, out cmsb_delta) &&
                tables["Cmq"].Get(alpha, out cmq) &&
                tables["Cmqlef_delta"].Get(alpha, out cmqlef_delta) &&
                tables["Cm_delta"].Get(alpha, out cm_delta) &&
                tables["Cmds_delta"].Get(alpha, ap.Control.Elevator, out cmds_delta)

                )
            {
                float cmlef_delta = cmlef - cm0;
                cmMoment = cm * gh_psi + cz * xc + cmlef_delta * ap.GLefRatioCompl
                + cmsb_delta * ap.GSbRatio + cq2v * (cmq + cmqlef_delta * ap.GLefRatioCompl)
                + cm_delta + cmds_delta;

                cmMoment *= ap.QBar * ap.Specification.S * ap.Specification.Cbar;

                return true;
            }
            else return false;
        }
        private bool GetNMoment(F16ForcesParameters ap, float cy, out float cnMoment)
        {
            var alpha = ap.AlphaDeg;
            var beta = ap.BetaDeg;
            float p = ap.St.AngularVeloctiy.x;
            float r = ap.St.AngularVeloctiy.z;
            float xc = ap.Specification.Xcgr - ap.Specification.Xcg;
            float cb = ap.Specification.Cbar / ap.Specification.B;
            float b2v = ap.Specification.B / (2 * ap.St.Speed);

            float cn0, cnlef, cnga, cngalef, cngr, cn, cnr, cnp, cnb_delta, cnrlef_delta, cnplef_delta;

            cnMoment = 0;

            if (tables["Cn"].Get(alpha, beta, 0, out cn0) &&
                tables["Cnlef"].Get(alpha, beta, out cnlef) &&
                tables["Cnga"].Get(alpha, beta, out cnga) &&
                tables["Cngalef"].Get(alpha, beta, out cngalef) &&
                tables["Cngr"].Get(alpha, beta, out cngr) &&
                tables["Cn"].Get(alpha, beta, ap.Control.Elevator, out cn) &&
                tables["Cnr"].Get(alpha, out cnr) &&
                tables["Cnp"].Get(alpha, out cnp) &&
                tables["Cnb_delta"].Get(alpha, out cnb_delta) &&
                tables["Cnrlef_delta"].Get(alpha, out cnrlef_delta) &&
                tables["Cnplef_delta"].Get(alpha, out cnplef_delta)

                )
            {
                float cnlef_delta = cnlef - cn0;
                float cnga_delta = cnga - cn0;
                float cngalef_delta = cngalef - cnlef - cnga_delta;
                float cngr_delta = cngr - cn0;

                cnMoment = cn + cnlef_delta * ap.GLefRatioCompl - cy * xc * cb
                + (cnga_delta + cngalef_delta * ap.GLefRatioCompl) * ap.GAilRatio
                + cngr_delta * ap.GRudRatio + b2v * ((cnr + cnrlef_delta * ap.GLefRatioCompl) * r
                + (cnp + cnplef_delta * ap.GLefRatioCompl) * p) + cnb_delta * beta;

                cnMoment *= ap.QBar * ap.Specification.S * ap.Specification.B;
                return true;

            }
            else return false;
        }
        private bool GetLMoment(F16ForcesParameters ap, out float clMoment)
        {
            var alpha = ap.AlphaDeg;
            var beta = ap.BetaDeg;
            float p = ap.St.AngularVeloctiy.x;
            float r = ap.St.AngularVeloctiy.z;
            float xc = ap.Specification.Xcgr - ap.Specification.Xcg;
            float cb = ap.Specification.Cbar / ap.Specification.B;
            float b2v = ap.Specification.B / (2 * ap.St.Speed);

            clMoment = 0;

            float cl0, cllef, clga, clgalef, clgr, cl, clr, clp, clb_delta, clrlef_delta, clplef_delta;

            if (
                tables["Cl"].Get(alpha, beta, 0, out cl0) &&
                tables["Cllef"].Get(alpha, beta, out cllef) &&
                tables["Clga"].Get(alpha, beta, out clga) &&
                tables["Clgalef"].Get(alpha, beta, out clgalef) &&
                tables["Clgr"].Get(alpha, beta, out clgr) &&
                tables["Cl"].Get(alpha, beta, ap.Control.Elevator, out cl) &&
                tables["Clr"].Get(alpha, out clr) &&
                tables["Clp"].Get(alpha, out clp) &&
                tables["Clb_delta"].Get(alpha, out clb_delta) &&
                tables["Clrlef_delta"].Get(alpha, out clrlef_delta) &&
                tables["Clplef_delta"].Get(alpha, out clplef_delta)
                )
            {
                float cllef_delta = cllef - cl0;
                float clga_delta = clga - cl0;
                float clgalef_delta = clgalef - cllef - clga_delta;
                float clgr_delta = clgr - cl0;

                clMoment = cl + cllef_delta * ap.GLefRatioCompl
                + (clga_delta + clgalef_delta * ap.GLefRatioCompl) * ap.GAilRatio
                + clgr_delta * ap.GRudRatio + b2v * ((clr + clrlef_delta * ap.GLefRatioCompl) * r
                + (clp + clplef_delta * ap.GLefRatioCompl) * p) + clb_delta * beta;

                clMoment *= ap.QBar * ap.Specification.S * ap.Specification.B;

                return true;
            }
            else return false;
        }
    }
}