﻿using Assets.Scripts.Aircraft;
using Assets.Scripts.Physics;
using UnityEngine;

namespace Assets.Scripts.F16Model
{
    public class F16Aircraft : Aircraft.Aircraft
    {
        private PhysicalModelEnum type;
        public F16Aircraft(PhysicalModelEnum _type)
        {
            type = _type;
            InitializeF16Constants(type);
        }
        public F16Aircraft(Vector3 translation, Quaternion rotation, PhysicalModelEnum type, float velocity = 0, Vector3? angularVelocity = null) : base(translation, rotation, velocity, angularVelocity)
        {
            InitializeF16Constants(type);
        }
        private void InitializeF16Constants(PhysicalModelEnum type)
        {
            if (type == PhysicalModelEnum.Lofi) Data = new LofiAerodynamicData();
            else Data = new HifiAerodynamicData();
            M = 9295.41f; // mass kg
            Specification = new F16AircraftSpecification();
            SetInertiaTensor();
            SetInverseInertiaTensor();
        }
        private void SetInertiaTensor()
        {
            Matrix4x4 Itemp;
            Itemp.m00 = Specification.Jx;
            Itemp.m01 = 0;
            Itemp.m02 = -Specification.Jxz;
            Itemp.m03 = 0;
            Itemp.m10 = 0;
            Itemp.m11 = Specification.Jy;
            Itemp.m12 = 0;
            Itemp.m13 = 0;
            Itemp.m20 = -Specification.Jxz;
            Itemp.m21 = 0;
            Itemp.m22 = Specification.Jz;
            Itemp.m23 = 0;
            Itemp.m30 = 0;
            Itemp.m31 = 0;
            Itemp.m32 = 0;
            Itemp.m33 = 1;           
            I = Itemp;
        }
        private void SetInverseInertiaTensor()
        {
            Matrix4x4 Itemp;
            var Id = Specification.Jx * Specification.Jz - Specification.Jxz * Specification.Jxz;

            Itemp.m00 = Specification.Jz / Id;
            Itemp.m01 = 0;
            Itemp.m02 = Specification.Jxz / Id;
            Itemp.m03 = 0;
            Itemp.m10 = 0;
            Itemp.m11 = 1.0f / Specification.Jy;
            Itemp.m12 = 0;
            Itemp.m13 = 0;
            Itemp.m20 = Specification.Jxz / Id;
            Itemp.m21 = 0;
            Itemp.m22 = Specification.Jx / Id;
            Itemp.m23 = 0;
            Itemp.m30 = 0;
            Itemp.m31 = 0;
            Itemp.m32 = 0;
            Itemp.m33 = Id;
            Iinv = Itemp;
        }
        public override bool GetForcesAndMoments(Parameters p, out Vector3 forces, out Vector3 moments)
        {
            Vector3 fab, mb;
            var pa = p as AircraftParameters;
            forces = Vector3.zero;
            moments = Vector3.zero;
            if (Data.GetAerodynamicForcesAndMoments(pa, out fab, out mb))
            {
                Vector3 wb = Quaternion.Inverse(p.St.Orientation) * new Vector3(0, 0, MathHelper.G * M);
                Vector3 tb = new Vector3(pa.Control.Thrust * Mathf.Cos(pa.Control.ThrustAngle), 0, pa.Control.Thrust * Mathf.Sin(pa.Control.ThrustAngle));
                Vector3 mt = Vector3.Cross(new Vector3(-pa.Specification.Xcgr, 0, 0), tb); // moment due to thrust        
                forces = fab + wb + tb;
                forces = new Vector3(forces.x, forces.y, forces.z);
                moments = (mb + mt);
                return true;
            }
            return false;
        }
        public PhysicalModelEnum GetPhysicalModel()
        {
            return type;
        }
    }
}