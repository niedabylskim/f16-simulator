﻿using Assets.Scripts.Aircraft;

namespace Assets.Scripts.F16Model
{
    public class F16AircraftSpecification : AircraftSpecification
    {
        public F16AircraftSpecification()
        {
            b = 9.144f * MathHelper.ScaleDownFactor;          // span - m 
            s = 27.87f * MathHelper.ScaleDownFactorSquared;         // planform area; m^2 
            cbar = 3.45f * MathHelper.ScaleDownFactor;         // mean aero chord; m 
            jy = 75674.0f * MathHelper.ScaleDownFactorSquared;     // kg-m^2 
            jxz = 1331.0f * MathHelper.ScaleDownFactorSquared;          // kg-m^2 
            jz = 85552.0f * MathHelper.ScaleDownFactorSquared;        // kg-m^2 
            jx = 12875.0f * MathHelper.ScaleDownFactorSquared;     // kg-m^2
            xcgr = 0.35f;      // reference center of gravity as a fraction of cbar 
            xcg = 0.30f;      // center of gravity as a fraction of cbar          
            maxAileronAngle = 21.5f;
            maxElevatorAngle = 25.0f;
            maxRudderAngle = 30.0f;
            maxLefAngle = 25.0f;
            maxThrust = 150000.0f * MathHelper.ScaleDownFactor; // For f16 thrust / weight = 1.096 which is about 150000 (max with equipment)
            maxThrustAngle = 10.0f;
            maxAlphaDeg = 45;
            minAlphaDeg = -20;
            maxBetaDeg = 30;
            minBetaDeg = -30;

        }

        public override float B { get { return b; } }

        public override float Cbar
        {
            get
            {
                return cbar;
            }
        }

        public override float Jx
        {
            get
            {
                return jx;
            }
        }

        public override float Jxz
        {
            get
            {
                return jxz;
            }
        }

        public override float Jy
        {
            get
            {
                return jy;
            }
        }

        public override float Jz
        {
            get
            {
                return jz;
            }
        }

        public override float MaxAileronAngle
        {
            get
            {
                return maxAileronAngle;
            }
        }

        public override float MaxAlphaDeg
        {
            get
            {
               return maxAlphaDeg;
            }
        }

        public override float MaxBetaDeg
        {
            get
            {
                return maxBetaDeg;
            }
        }

        public override float MaxElevatorAngle
        {
            get
            {
                return maxElevatorAngle;
            }
        }

        public override float MaxLefAngle
        {
            get
            {
                return maxLefAngle;
            }
        }

        public override float MaxRudderAngle
        {
            get
            {
                return maxRudderAngle;
            }
        }

        public override float MaxThrust
        {
            get
            {
                return maxThrust;
            }
        }

        public override float MaxThrustAngle
        {
            get
            {
                return maxThrustAngle;
            }
        }

        public override float MinAlphaDeg
        {
            get
            {
                return minAlphaDeg;
            }
        }

        public override float MinBetaDeg
        {
            get
            {
                return minBetaDeg;
            }
        }

        public override float S
        {
            get
            {
                return s;
            }
        }

        public override float Xcg
        {
            get
            {
                return xcg;
            }
        }

        public override float Xcgr
        {
            get
            {
                return xcgr;
            }
        }
    }
}