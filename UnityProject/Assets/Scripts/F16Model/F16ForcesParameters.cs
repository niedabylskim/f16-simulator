﻿using Assets.Scripts.Aircraft;
using UnityEngine;

namespace Assets.Scripts.F16Model
{
    public class F16ForcesParameters : AircraftParameters
    {
        public float GSbRatio { get; set; }
        public float GLefRatio { get; set; }
        public float GAilRatio { get; set; }
        public float GRudRatio { get; set; }
        public float GLefRatioCompl { get; set; }
        public float QBar { get; set; }
        public float Ps { get; set; }
        public float AlphaDeg { get; set; }
        public float BetaDeg { get; set; }

        public F16ForcesParameters(AircraftParameters parent)
        {
            Specification = parent.Specification;
            St = parent.St;
            Control = parent.Control;
            AlphaDeg = St.Alpha * Mathf.Rad2Deg;
            BetaDeg = St.Beta * Mathf.Rad2Deg;
            GSbRatio = 0;
            float[] atmosphereConditions;
            Control.Lef = MathHelper.CalculateLeadingEdgeFlap(St.Speed, -St.Position.z, Specification.S, AlphaDeg, out atmosphereConditions);
            QBar = atmosphereConditions[0];
            Ps = atmosphereConditions[1];
            GLefRatio = Control.Lef / Specification.MaxLefAngle;
            GLefRatioCompl = 1 - GLefRatio;
            GAilRatio = Control.Aileron / Specification.MaxAileronAngle;
            GRudRatio = Control.Rudder / Specification.MaxRudderAngle;
        }
    }
}