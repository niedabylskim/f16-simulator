﻿using Assets.Scripts.MissilesDevelop;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.MissilesTests
{
    public class RandomPosTest : MonoBehaviour
    {
        List<List<Vector3>> testData;
        public Target target;
        public GameObject spherePoint;
        public NavAdder[] missiles;
        private int numberOfTests;
        private string nameEnd;
        private int numOfPoints = 300;
        private int numOfDirs = 3;
        private int[] R;
        private int Angle = 5;
        private int[] N;
        private int Testnr = 1;
        private float reaktDistance = 0;

        public void Start()
        {
            R = new int[] { 1000 };
            N = new int[] { 5 };
            InitializeMissiles();
            //foreach (var r in R)
            //    foreach (var n in N)
            //    {
            //        reaktDistance = r / 15;
            //        numberOfTests = 0;
            //        nameEnd = "_Test" + Testnr + "_R" + r + "_A" + Angle + "_N" + n + "_Dist" + reaktDistance + ".txt";
            //        //GenerateData(numOfPoints, numOfDirs, r, Angle);
            //        ReadDataFromFile(@"Datapoints\dataPoints" + nameEnd);
            //        StartTests();
            //    }
            //DisplayHedgeHog();
            //int r = R[0];
            //int n = N[0];
            //reaktDistance = r / 15;
            //nameEnd = "_Test" + Testnr + "_R" + r + "_A" + Angle + "_N" + n + "_Dist" + reaktDistance + ".txt";
            //ReadDataFromFile(@"Datapoints\dataPoints" + nameEnd);
            //StartCoroutine(VisualizeTest(31));
        }

        void InitializeMissiles()
        {
            foreach (var m in missiles)
            {
                m.StartRequest();
            }
        }

        private void DisplayHedgeHog()
        {
            for (int i = 0; i < testData.Count; i++)
            {
                Vector3 pointOnSphere = testData[i][0];
                var obj = Instantiate(spherePoint, pointOnSphere, Quaternion.identity);
                for (int j = 1; j < testData[i].Count; j++)
                {                    
                    Vector3 localAngles = testData[i][j];
                    obj.transform.LookAt(target.transform.position);
                    obj.transform.localRotation *= Quaternion.Euler(localAngles);
                    Debug.DrawLine(obj.transform.position, obj.transform.position + obj.transform.forward * 100, Color.red, 50);
                }
            }
        }

        void StartTests()
        {
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            float[,] tab = new float[numberOfTests * missiles.Length, 4];
            int numOfTestTotal = 0;
            foreach (var m in missiles)
            {
                int currentTest = 0;
                for (int i = 0; i < testData.Count; i++)
                {
                    Vector3 pointOnSphere = testData[i][0];
                    //var obj = Instantiate(spherePoint, pointOnSphere, Quaternion.identity);
                    for (int j = 1; j < testData[i].Count; j++)
                    {
                        TestStep(m, pointOnSphere, testData[i][j]);
                        target.StartRequest();
                        while (!m.DoNextStepOfSimulation())
                        {
                            target.DoNextStepTest1(m.transform.position, reaktDistance);
                        }
                        float time, dist;
                        m.GetTimeAndDistance(out time, out dist);

                        tab[numOfTestTotal, 0] = currentTest++;
                        tab[numOfTestTotal, 1] = (int)m.navType;
                        tab[numOfTestTotal, 2] = time;
                        tab[numOfTestTotal++, 3] = dist;
                    }
                }
            }
            watch.Stop();
            Debug.Log(watch.ElapsedMilliseconds);
            ExportToFile(@"Results\results" + nameEnd, tab);
        }

        public void ExportToFile(string path, float[,] tab)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < tab.GetLength(0); i++)
            {
                for (int j = 0; j < tab.GetLength(1); j++)
                    sb.Append(tab[i, j] + "\t");
                if (i != tab.GetLength(0) - 1) sb.Append("\n");
            }
            File.WriteAllText(path, sb.ToString());
        }

        void TestStep(NavAdder m, Vector3 pos, Vector3 rot)
        {
            m.transform.position = pos;
            m.transform.LookAt(target.transform.position);
            m.transform.localRotation *= Quaternion.Euler(rot);
            m.Initialize();
            
        }

        IEnumerator VisualizeTest(int testNum)//, int index = 0)
        {
            Vector3 pos, rot;
            FindData(testNum, out pos, out rot);
            pos = new Vector3(800, 1, 0);
            foreach(var m in missiles)
                TestStep(m, pos, rot);
            target.StartRequest();
            var obj = Instantiate(spherePoint, pos, Quaternion.identity);
            target.StartRequest();
            Vector3 oldPosMissile = pos;
            Vector3 oldPosTarget = target.transform.position;
            int fr = 0;
            bool loop = false;
            while (!loop)
            {
                loop = true;
                foreach (var m in missiles)
                {
                    oldPosMissile = m.transform.position;
                    loop = m.DoNextStepOfSimulation() && loop;
                    Debug.DrawLine(oldPosMissile, m.transform.position, m.Color, 20);
                }
                //oldPosMissile = missiles[index].transform.position;
                //missiles[index].DoNextStepOfSimulation();
                //Debug.DrawLine(oldPosMissile, missiles[index].transform.position, missiles[index].Color, 10);
                fr++;
                target.DoNextStepTest1(missiles[0].transform.position, reaktDistance);
                Debug.DrawLine(oldPosTarget, target.transform.position, Color.yellow, 20);
                oldPosTarget = target.transform.position;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                //float time, dist;
                //missile.GetTimeAndDistance(out time, out dist);
                //Debug.Log("Time: " + time + " dist: " + dist);
            }
        }
        
        private void FindData(int testNum, out Vector3 pos, out Vector3 rot)
        {
            int indexPos = testNum / numOfDirs;
            int indexRot = (testNum % numOfDirs) + 1;
            pos = testData[indexPos][0];
            rot = testData[indexPos][indexRot];
        }

        void GenerateData(int numOfPoints, int numOfDirs, float r, int angleVar)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < numOfPoints; i++)
            {
                sb.Append(MathHelper.GetPositionOnSphere(r).ToString("F3"));
                for (int j = 0; j < numOfDirs; j++)
                {
                    sb.Append(" ");
                    sb.Append((new Vector3(MathHelper.GetRandomAngle(-angleVar, angleVar),
                        MathHelper.GetRandomAngle(-angleVar, angleVar), 
                        0)).ToString("F3"));
                }
                if(i != numOfPoints - 1)sb.Append("\n");
            }

            File.WriteAllText(@"Datapoints\dataPoints" + nameEnd, sb.ToString());
        }

        void ReadDataFromFile(string path)
        {
            testData = new List<List<Vector3>>();
            var text = File.ReadAllText(path);
            text = text.Replace(",", "");
            text = text.Replace("(", "");
            text = text.Replace(")", "");
            var lines = text.Split('\n');
            foreach(var l in lines)
            {
                var data = l.Split(' ');
                List<Vector3> dataPoints = new List<Vector3>();
                var pointOnSphere = new Vector3(float.Parse(data[0]), float.Parse(data[1]), float.Parse(data[2]));
                dataPoints.Add(pointOnSphere);
                var numOfdirs = (data.Length - 3) / 3;
                numberOfTests += numOfdirs;
                for(int i = 1; i <= numOfdirs; i++)
                {
                    int k = 3 * i;
                    var dir = new Vector3(float.Parse(data[k]), float.Parse(data[k + 1]), float.Parse(data[k + 2]));
                    dataPoints.Add(dir);
                }
                testData.Add(dataPoints);
            }
        }

        
    }
}
