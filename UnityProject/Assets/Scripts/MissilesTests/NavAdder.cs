﻿using Assets.Scripts.Missiles;
using Assets.Scripts.Missiles.Guidance;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.MissilesDevelop
{
    public class NavAdder : MonoBehaviour
    {
        public NavType navType;
        public Target target;
        public Color Color;
        private MissileNav guidance;
        private TargetData targetData;
        private TargetData oldTargetData;
        private float totalDistance = 0;
        private float totalTime;
        private int frame = 0;
        bool firstData = true;
        private float testMaxTime = 20;
        private float testMaxSteps;
        private bool wasCollision;

        public void StartRequest()
        {
            targetData = new TargetData();
            oldTargetData = new TargetData();
            CreateGuidanceType(navType);
            testMaxSteps = testMaxTime / Time.fixedDeltaTime;
        }

        public void Initialize()
        {
            guidance.Initialize(transform.position, transform.forward, targetData);
            totalDistance = 0;
            totalTime = 0;
            firstData = true;
            wasCollision = false;
        }

        public bool DoNextStepOfSimulation()
        {
            if (wasCollision) return true;
            CalculateTargetData();
            //if (frame++ < 3) return false;
            totalTime++;
            if (totalTime > testMaxSteps) return true;
            Vector3 oldPos = transform.position;
            guidance.Integrate(targetData);
            transform.position = guidance.Position;
            totalDistance += Vector3.Distance(transform.position, oldPos);
            transform.LookAt(guidance.Position + guidance.Velocity);
            //Debug.DrawLine(oldPos, transform.position, Color, 15);
            wasCollision = CheckCollision();
            return wasCollision;
        }

        private bool CheckCollision()
        {
            return Vector3.Distance(transform.position, target.transform.position) < 20 * MathHelper.ScaleDownFactor;
        }
        public void GetTimeAndDistance(out float time, out float dist)
        {
            time = totalTime * Time.fixedDeltaTime;
            dist = totalDistance;
        }
        private void CreateGuidanceType(NavType type)
        {
            switch (type)
            {
                case NavType.PursuitNav:
                    guidance = gameObject.AddComponent<PursuitNav>();
                    break;
                case NavType.KalmanNav:
                    guidance = gameObject.AddComponent<KalmanNav>();
                    break;
                case NavType.PnApnVm:
                    guidance = gameObject.AddComponent<PnApnVm>();
                    break;
                case NavType.PnVm:
                    guidance = gameObject.AddComponent<PnVm>();
                    break;
                case NavType.ApnVm:
                    guidance = gameObject.AddComponent<ApnVm>();
                    break;
            }
        }

        public void CalculateTargetData()
        {
            oldTargetData.Position = targetData.Position;
            oldTargetData.Velocity = targetData.Velocity;
            oldTargetData.Acceleration = targetData.Acceleration;

            if (firstData)
            {
                Vector3 targetStartingVelocity = target.Velocity;
                Vector3 targetStartPosition = target.StartPoistion;
                targetData.Position = targetStartPosition;
                targetData.Velocity = targetStartingVelocity;
                targetData.Acceleration = Vector3.zero;
                firstData = false;
            }
            else
            {
                targetData.Position = target.transform.position;
                targetData.Velocity = (targetData.Position - oldTargetData.Position) / Time.fixedDeltaTime;
                targetData.Acceleration = (targetData.Velocity - oldTargetData.Velocity) / Time.fixedDeltaTime;
            }
        }
    }
}
