﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    private float speed = 50f;
    private float maxSpeed = 100f;

    public Vector3 prevPosition;
    public Vector3 prevVelocity;
    public Vector3 Velocity;
    public Vector3 Acceleration;
    public Vector3 StartPoistion = new Vector3(0, 1, 0);
    private Vector3 oldMissilePos;
    int count = 0;
    // Use this for initialization
    public void StartRequest () {

        transform.position = StartPoistion;
        transform.rotation = Quaternion.identity;
        Velocity = transform.forward * speed;
        Acceleration = Vector3.zero;
    }
	
	// Update is called once per frame
	public void DoNextStepTest0()
    {
        prevPosition = transform.position;
        Integrate(Acceleration);
    }

    //niejednostajny
    public void DoNextStepTest1(Vector3 missilePos, float dist)
    {
        //if (Vector3.Distance(transform.position, missilePos) < dist)// && Acceleration == Vector3.zero)
        {
            
            var tan = Random.Range(25, 30);
            var fow = Random.Range(40, 100);
            Acceleration = Quaternion.Euler(0, Random.Range(-30, 30), 0) * transform.right * tan + transform.forward * fow;
        }
        Integrate(Acceleration);
    }

    //nagla ucieczka
    public void DoNextStepTest2(Vector3 missilePos, float dist)
    {
        if (Vector3.Distance(transform.position, missilePos) < dist)// && Acceleration == Vector3.zero)
        {
            maxSpeed = 100f;
            var tan = Random.Range(25, 30);
            var fow = 180;
            Acceleration = Quaternion.Euler(0, Random.Range(20, 30), 0) * transform.right * tan + transform.forward * fow;
        }
        Integrate(Acceleration);
    }

    //sterowanie reczne
    public void DoNextStepTest3(Vector3 missilePos, float dist)
    {
        Acceleration = pomAcc;
        Integrate(Acceleration);
    }
    Vector3 pomAcc;
    void Update()
    {
        if (Input.GetKeyDown("k"))  maxSpeed = 100f;
        float scaling = 200;
        float x = Input.GetAxis("ElevatorAxis");
        float z = -Input.GetAxis("AileronAxis");
        pomAcc = new Vector3(z, 0, x) * scaling;
        //Debug.DrawLine(transform.position, transform.position + Acceleration, Color.cyan);
    }

    private void Integrate(Vector3 acc)
    {
        Velocity += acc * Time.fixedDeltaTime;
        if (Velocity.magnitude > maxSpeed)
            Velocity = Velocity.normalized * maxSpeed;
        transform.LookAt(transform.position + Velocity);
        transform.position += Velocity * Time.fixedDeltaTime;
    }
}
