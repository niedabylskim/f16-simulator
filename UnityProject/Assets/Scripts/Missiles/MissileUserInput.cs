﻿using Assets.Scripts.Missiles.Guidance;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Assets.Scripts.Missiles
{
    public class MissileUserInput : MonoBehaviour
    {
        Missile player;
        NavType type;

        Text missileTypeText;
        CanvasGroup missileImageCanvasGroup;
        Image missileImage;
        int enumLength;
        bool isMissileRespawning = false;
        float canvasGroupAlphaBegin, canvasGroupAlphaEnd;
        void Awake()
        {
            canvasGroupAlphaEnd = 0.2f;
            player = GetComponent<Missile>();
            var canvas = GameObject.Find("Canvas");
            missileTypeText = canvas.GetComponentsInChildren<Text>()[2];
            missileImage = canvas.GetComponentsInChildren<Image>()[2];
            missileImageCanvasGroup = missileImage.GetComponent<CanvasGroup>();
            missileTypeText.text = type.ToString();
            enumLength = System.Enum.GetNames(typeof(NavType)).Length;
            canvasGroupAlphaBegin = missileImageCanvasGroup.alpha;
        }

        void Update()
        {
            if (!player.isLaunched) GetUserInput();
        }

        public void GetUserInput()
        {
            if (Input.GetButtonDown("FireMissile") && !isMissileRespawning)
            {
                if (player.Launch(type))
                {
                    isMissileRespawning = true;
                    missileImageCanvasGroup.alpha = canvasGroupAlphaEnd;
                    StartCoroutine(ChangeOpacityCanvasGroup(player.MissileLifeTime, canvasGroupAlphaEnd, canvasGroupAlphaBegin));
                }
            }
            if (Input.GetButtonDown("ChangeMissileType"))
            {
                type++;
                if ((int)type >= enumLength) type = 0;
                missileTypeText.text = type.ToString();
            }
        }

        private IEnumerator ChangeOpacityCanvasGroup(float waitTime, float alphaFrom, float alphaTo)
        {
            float waitTimeOutline = 0.5f;
            float timeStep = 0.2f;
            int step = 1;
            int stepNums = (int)((waitTime - waitTimeOutline )/ timeStep);

            float alphaStep = (alphaTo - alphaFrom) / stepNums;
            while (step <= stepNums)
            {
                missileImageCanvasGroup.alpha = alphaFrom + (alphaStep * step++);
                yield return new WaitForSeconds(timeStep);
            }
            missileImageCanvasGroup.alpha = alphaTo;
            StartCoroutine(ChangeOpacityOutline(waitTimeOutline, 1, 0));
        }

        private IEnumerator ChangeOpacityOutline(float waitTime, float alphaFrom, float alphaTo)
        {
            float timeStep = 0.2f;
            int step = 1;
            int stepNums = (int)(waitTime / timeStep);
            float alphaStep = (alphaFrom - alphaTo) / stepNums;
            var outline = missileImage.GetComponent<Outline>();
            while (step <= stepNums)
            {
                outline.effectColor = new Color(1, 0.9f, 0.3f, 1 - (alphaStep * step++));
                yield return new WaitForSeconds(timeStep);
            }
            outline.effectColor = new Color(1, 0.9f, 0.3f, alphaTo);
            isMissileRespawning = false;
        }
    }
}
