﻿using Assets.Scripts.Missiles.Guidance;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Missiles
{
    public class Missile : NetworkBehaviour
    {
        [SyncVar]
        public Transform Parent;
        public GameObject Explosion;
        public GameObject Trail;
        public bool isLaunched;
        private bool isTarget;
        public NavType guidanceType;

        private Transform targetTransform;
        private MissileNav guidance;
        private TargetData targetData;
        private TargetData oldTargetData;
        public float MissileLifeTime { get; private set; }
        private Vector3 offset = new Vector3(0.426f, -0.644f, -3.04f);
        private Quaternion rotationOffset = Quaternion.Euler(0, 90f, 0);
        private float explosionTime;
        private AudioSource sourceLaunch;
        private MeshRenderer[] childrenMeshRenderers;

        private void Awake()
        {
            childrenMeshRenderers = GetComponentsInChildren<MeshRenderer>();
            sourceLaunch = GetComponent<AudioSource>();
            sourceLaunch.volume = SettingsManager.Instance.SfxVolume;
        }

        public override void OnStartAuthority()
        {
            base.OnStartAuthority();
            gameObject.AddComponent<MissileUserInput>();
        }

        private void Start()
        {
            MissileLifeTime = 10;
            targetData = new TargetData();
            oldTargetData = new TargetData();
            explosionTime = Explosion.GetComponentInChildren<ParticleSystem>().main.duration;
        }


        void FixedUpdate()
        {
            Vector3 oldPos = transform.position;
            if (isTarget) CalculateTargetData();
            if (!isLaunched)
            {
                transform.position = Parent.position + Parent.TransformVector(offset);
                transform.rotation = Parent.rotation * rotationOffset;
                return;
            }
            else
            {
                guidance.Integrate(targetData);
                transform.position = guidance.Position;
                transform.LookAt(guidance.Position + guidance.Velocity);
                CheckCollision();
            }
            Debug.DrawLine(oldPos, transform.position, guidance.Color, MissileLifeTime + 5);
        }

        private void CheckCollision()
        {
            if (Vector3.Distance(transform.position, targetTransform.position) < 20 * MathHelper.ScaleDownFactor)
            {
                Debug.Log("Explosion!! " + Multiplayer.MultiplayerManager.GetTime());
                var expl = Instantiate(Explosion, transform.position + targetTransform.right * 3, transform.rotation);
                Destroy(expl, explosionTime);
                var trail = GetComponentInChildren<TrailRenderer>();
                if (trail != null)
                {
                    trail.gameObject.transform.parent = null;
                }
                ChangeMeshVisibility(false);
                isLaunched = false;
            }
        }

        private void ChangeMeshVisibility(bool value)
        {
            for (int i = 0; i < childrenMeshRenderers.Length; i++)
                childrenMeshRenderers[i].enabled = value;
        }

        [Server]
        void RespawnMissile()
        {
            RpcSetLaunchToFalse();
        }

        [ClientRpc]
        void RpcSetLaunchToFalse()
        {
            isLaunched = false;
            ChangeMeshVisibility(true);
            Destroy(guidance);
        }

        public bool Launch(NavType type)
        {
            if (!SetLaunchData(type)) return false;
            CmdSetLaunch(type);
            sourceLaunch.Play();
            return true;
        }
        private bool SetLaunchData(NavType type)
        {
            isTarget = FindTarget();
            if (!isTarget) return false;
            CreateGuidanceType(type);
            isLaunched = true;
            return true;
        }
        private void CreateGuidanceType(NavType type)
        {
            switch (type)
            {
                case NavType.PursuitNav:
                    guidance = gameObject.AddComponent<PursuitNav>();
                    break;
                case NavType.KalmanNav:
                    guidance = gameObject.AddComponent<KalmanNav>();
                    break;
                case NavType.PnVm:
                    guidance = gameObject.AddComponent<PnVm>();
                    break;
                case NavType.ApnVm:
                    guidance = gameObject.AddComponent<ApnVm>();
                    break;
                case NavType.PnApnVm:
                    guidance = gameObject.AddComponent<PnApnVm>();
                    break;
            }
            var trail = Instantiate(Trail, transform);
            trail.GetComponent<TrailRenderer>().colorGradient = CreateGradient(guidance.Color);
            Destroy(trail, MissileLifeTime * 1.5f);
            guidance.Initialize(Parent.position + Parent.TransformVector(offset), Parent.right, targetData);
        }

        private Gradient CreateGradient(Color color)
        {
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(1, 0), new GradientAlphaKey(1, 0.6f), new GradientAlphaKey(0, 1) }
                );
            return gradient;
        }

        [Command]
        private void CmdSetLaunch(NavType type)
        {
            if (!hasAuthority) SetLaunchData(type);
            RpcOnGuidanceTypeChange(type);
            Invoke("RespawnMissile", MissileLifeTime);
        }

        [ClientRpc]
        public void RpcOnGuidanceTypeChange(NavType val)
        {
            guidanceType = val;
            if (!hasAuthority && !isServer) SetLaunchData(val);
        }

        private bool FindTarget()
        {
            if (hasAuthority)
            {
                var target = FindObjectOfType<Aircraft.AircraftUserObserved>();
                if (target == null) return false;
                targetTransform = target.transform;
            }
            else
            {
                var target = FindObjectOfType<Aircraft.AircraftClientPredicted>();
                if (target == null) return false;
                targetTransform = target.transform;
            }
            return true;
        }

        public void CalculateTargetData()
        {
            oldTargetData.Position = targetData.Position;
            oldTargetData.Velocity = targetData.Velocity;
            oldTargetData.Acceleration = targetData.Acceleration;

            if (targetTransform == null)
            {
                isLaunched = false;
                Destroy(guidance);
                return;
            }

            targetData.Position = targetTransform.position;
            targetData.Velocity = (targetData.Position - oldTargetData.Position) / Time.fixedDeltaTime;
            targetData.Acceleration = (targetData.Velocity - oldTargetData.Velocity) / Time.fixedDeltaTime;
        }
    }
}
