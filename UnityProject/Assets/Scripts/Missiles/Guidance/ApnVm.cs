﻿using UnityEngine;

namespace Assets.Scripts.Missiles.Guidance
{
    public class ApnVm : PnBase
    {
        public override Color Color { get { return Color.yellow; } }
        public override void CalculateAcceleration(TargetData targetData)
        {
            base.CalculateAcceleration(targetData);
            var accVm = -N * Vr.magnitude * Vm.normalized;
            var accAug = GetProjectedAcceleration(targetData, Vm) * N / 2f;
            Acceleration = Vector3.Cross(accVm, Om) + accAug;
        }
    }
}
