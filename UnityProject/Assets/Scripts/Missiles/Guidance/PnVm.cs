﻿using Assets.Scripts.Missiles;
using Assets.Scripts.Missiles.Guidance;
using System;
using UnityEngine;

namespace Assets.Scripts.Missiles.Guidance
{
    public class PnVm : PnBase
    {
        public override Color Color { get { return Color.magenta; } }
        public override void CalculateAcceleration(TargetData targetData)
        {
            base.CalculateAcceleration(targetData);
            Acceleration = Vector3.Cross(-N * Vr.magnitude * Vm.normalized, Om);
            
        }
    }
}
