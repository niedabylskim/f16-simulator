﻿using UnityEngine;

namespace Assets.Scripts.Missiles.Guidance
{
    public abstract class PnBase : MissileNav
    {
        protected const int N = 5; //Proportional navigation constant
        protected Vector3 Vt;
        protected Vector3 Vm;
        protected Vector3 Vr;
        protected Vector3 R;
        protected Vector3 Om;
        protected float closeRangeDistance = 1000 * MathHelper.ScaleDownFactor;
        public override void CalculateAcceleration(TargetData targetData)
        {
            Vt = targetData.Velocity;
            Vm = Velocity;
            Vr = Vt - Vm;
            R = targetData.Position - transform.position;
            Om = Vector3.Cross(R, Vr) / Vector3.Dot(R, R);
        }

        protected Vector3 GetProjectedAcceleration(TargetData targetData, Vector3 vectorOn)
        {
            var proj = Vector3.Project(targetData.Acceleration, vectorOn);
            return targetData.Acceleration - proj;
        }
    }
}
