﻿using Assets.Scripts.Physics;
using System;
using UnityEngine;

namespace Assets.Scripts.Missiles.Guidance
{
    public class KalmanNav : MissileNav
    {
        private KalmanFilter kalman;
        private double dt = 1;
        private float kalmanFactor = 5 * MathHelper.ScaleDownFactor;
        public override Color Color { get { return Color.blue; } }

        public override void CalculateAcceleration(TargetData targetData)
        {
            var R = Vector3.Distance(targetData.Position, transform.position);
            var Vr = targetData.Velocity - Velocity;
            dt = kalmanFactor * R / Vr.magnitude;
            Vector3 predictedTarget = kalman.Predict(targetData.Position, targetData.Velocity, dt);
            var newVelocity = (predictedTarget - transform.position).normalized * Velocity.magnitude;
            Acceleration = (newVelocity - Velocity) / Time.fixedDeltaTime;
        }

        public override void Initialize(Vector3 startPosition, Vector3 initialVelocityDirection, TargetData targetData)
        {
            base.Initialize(startPosition, initialVelocityDirection, targetData);
            kalman = new KalmanFilter(targetData.Position, targetData.Velocity, Time.fixedDeltaTime);
        }
    }
}