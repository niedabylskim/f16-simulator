﻿using System;
using UnityEngine;

namespace Assets.Scripts.Missiles.Guidance
{
    public abstract class MissileNav : MonoBehaviour
    {
        public Vector3 Position { get; private set; }
        public Vector3 Velocity { get; private set; }
        public Vector3 Acceleration { get; protected set; }
        protected float initialSpeed = 1500 * MathHelper.ScaleDownFactor;
        protected float maxSpeed = 1500 * MathHelper.ScaleDownFactor;
        protected float longitudinalAcceleration = 50 * MathHelper.ScaleDownFactor;
        protected float maxAcceleration = 1500 * MathHelper.ScaleDownFactor;
        protected const float maxTurnRate = 70f ; //150 degrees per second
        protected float maxTurnPerTimeStep;

        public abstract Color Color { get; }
        //private AudioSource sourceLaunch, sourceExplosion;

        public virtual void Initialize(Vector3 startPosition, Vector3 initialVelocityDirection, TargetData targetData)
        {
            Position = startPosition;
            Velocity = initialVelocityDirection * initialSpeed;
            Acceleration = Vector3.zero;
            maxTurnPerTimeStep = maxTurnRate * Mathf.Deg2Rad * Time.fixedDeltaTime;
        }

        public void Integrate(TargetData targetData)
        {
            CalculateAcceleration(targetData);
            //ApplyLongitudinalAcceleration();
            //if (Acceleration.magnitude > maxAcceleration)
            //{
            //    //Debug.Log(gameObject.name + " " + Acceleration.magnitude);
            //    Acceleration = Acceleration.normalized * maxAcceleration;
            //}
            Vector3 oldVelocity = Velocity;
            Velocity += Acceleration * Time.fixedDeltaTime;
            ClampVelocity(oldVelocity);
            Position += Velocity * Time.fixedDeltaTime;
        }

        public abstract void CalculateAcceleration(TargetData targetData);
        
        private void ClampVelocity(Vector3 oldVelocity)
        {
            float magnitude = Velocity.magnitude;
            var angle = Vector3.Angle(oldVelocity, Velocity);
            if (angle > maxTurnPerTimeStep)
            {
                Velocity = Vector3.RotateTowards(oldVelocity.normalized, Velocity, maxTurnPerTimeStep, 0);
            }
            Velocity = Velocity.normalized * (magnitude < maxSpeed ? magnitude : maxSpeed);
        }

        private void ApplyLongitudinalAcceleration()
        {
            var accM = Acceleration.magnitude;
            if (accM > maxAcceleration)
            {
                Acceleration = Acceleration.normalized * maxAcceleration;
                return;
            }
            var maxAccGain = maxAcceleration - accM;
            var lonAccMag = Mathf.Min(maxAccGain, longitudinalAcceleration - accM);
            Acceleration += transform.forward * lonAccMag;
        }
    }
}
