﻿using UnityEngine;

namespace Assets.Scripts.Missiles.Guidance
{
    public class PnApnVm : PnBase
    {
        public override Color Color { get { return Color.green; } }
        public override void CalculateAcceleration(TargetData targetData)
        {
            base.CalculateAcceleration(targetData);
            if (R.magnitude > closeRangeDistance) Acceleration = Vector3.Cross(-N * Vr.magnitude * Vm.normalized, Om);
            else
            {
                Acceleration = Vector3.Cross(-N * Vr.magnitude * Vm.normalized, Om)
                    + GetProjectedAcceleration(targetData, Vm) * N / 2f;
            }
        }
    }
}
