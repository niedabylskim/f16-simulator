﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Missiles.Guidance
{
    public enum NavType
    {
        PursuitNav,
        KalmanNav,
        PnVm,
        ApnVm,
        PnApnVm
    }
}
