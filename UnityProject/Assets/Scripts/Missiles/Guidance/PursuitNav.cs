﻿using UnityEngine;
using System;

namespace Assets.Scripts.Missiles.Guidance
{
    public class PursuitNav : MissileNav
    {
        public override Color Color { get { return Color.red; } }

        public override void CalculateAcceleration(TargetData targetData)
        {
            Vector3 planeVec = targetData.Position;
            var newVelocity = (planeVec - transform.position).normalized * Velocity.magnitude;
            Acceleration = (newVelocity - Velocity) / Time.fixedDeltaTime;
        }

    }
}