﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Assets.Scripts.Physics;
using Assets.Scripts.Aircraft;
using Assets.Scripts.Missiles;

namespace Assets.Scripts.Aircraft
{
    public class AircraftClient : NetworkBehaviour
    {
        public GameObject Rudder;
        public GameObject AileronLeft;
        public GameObject AileronRight;
        public GameObject ElevatorLeft;
        public GameObject ElevatorRight;
        public GameObject LefLeft;
        public GameObject LefRight;
        public ParticleSystem ThrustFlame;

        [SyncVar(hook = "OnServerUpdate")]
        public State serverState;

        public Aircraft jet;

        public bool hasNoFlightError = true;
        public bool areAnglesInLimits = true;
        private bool wasPlayerAlreadyRespawned = false;

        private Text positionText, controlText;
        private FlapController flapController;

        protected const string format = "F2";
        private RectTransform targetOnScreen;
        private GameObject targetOnScreenImage;
        IClientStateHandler stateHandler;
        MultiplayerServer server;
        AircraftClientInput userInput;

        public GameObject multiplayerMissilePrefab;

        [Server]
        void AwakeOnServer()
        {
            server = gameObject.AddComponent<MultiplayerServer>();
        }

        protected virtual void Awake()
        {
            GetComponent<AudioSource>().volume = SettingsManager.Instance.SfxVolume;
        }

        public override void OnStartClient()
        {
            RespawnPlayer();
        }

        void Start()
        {
            if (isServer) AwakeOnServer();
            if (!isLocalPlayer)
            {
                stateHandler = gameObject.AddComponent<AircraftUserObserved>();
                var img = gameObject.GetComponentInChildren<Canvas>().GetComponentInChildren<Image>();
                targetOnScreenImage = img.gameObject;
                targetOnScreen = img.GetComponent<RectTransform>();

                return;
            }

            gameObject.GetComponentInChildren<Canvas>().gameObject.SetActive(false);

            stateHandler = gameObject.AddComponent<AircraftClientPredicted>();
            flapController = new FlapController(Rudder, AileronLeft, AileronRight, ElevatorLeft, ElevatorRight, LefLeft, LefRight);
            userInput = gameObject.AddComponent<AircraftClientInput>();
            var texts = GameObject.Find("Canvas").GetComponentsInChildren<Text>();
            positionText = texts[0];
            controlText = texts[1];
            CmdSpawnMissile();
        }

        [Command]
        void CmdSpawnMissile()
        {
            Debug.Log("Spawn");
            GameObject mis = Instantiate(multiplayerMissilePrefab);
            mis.GetComponent<Missile>().Parent = transform;

            NetworkServer.SpawnWithClientAuthority(mis, connectionToClient.playerControllers[0].gameObject);
        }

        void FixedUpdate()
        {
            RenderAircraft();
            if (isLocalPlayer)
            {
                SetInfoText();
                if (!hasNoFlightError && !wasPlayerAlreadyRespawned)
                {
                    Invoke("RespawnPlayer", 3);
                    wasPlayerAlreadyRespawned = true;
                    hasNoFlightError = false;
                }
            }
            else
            {
                Vector3 viewportPoint = Camera.allCameras[0].WorldToViewportPoint(transform.position);
                float x = Mathf.Clamp(viewportPoint.x, 0, 1);
                float y = Mathf.Clamp(viewportPoint.y, 0, 1);
                float z = viewportPoint.z;
                bool isonScreen = z > 0;
                if (isonScreen)
                {
                    Vector2 anchor = new Vector2(x, y);
                    targetOnScreenImage.SetActive(true);
                    targetOnScreen.anchorMin = anchor;
                    targetOnScreen.anchorMax = anchor;
                }
                else targetOnScreenImage.SetActive(false);
            }
        }

        private void RenderAircraft()
        {
            var currentState = stateHandler.GetCurrentState();
            var nr = currentState.Orientation;
            var np = currentState.Position;

            if (!isLocalPlayer)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, new Quaternion(nr.x, -nr.z, nr.y, nr.w), 0.2f);
                if (hasNoFlightError)
                    transform.position = Vector3.Lerp(transform.position, new Vector3(np.x, -np.z, np.y), 0.02f);
                else transform.position = new Vector3(np.x, -np.z, np.y);
            }
            else
            {
                transform.rotation = new Quaternion(nr.x, -nr.z, nr.y, nr.w);
                transform.position = new Vector3(np.x, -np.z, np.y);
            }
            if (isLocalPlayer && jet != null)
            {
                flapController.Rotate(jet.Control, transform.rotation);
                jet.St = currentState;
            }
        }

        protected virtual void GetInfoText(out string positionText, out string controlText)
        {
            StringBuilder sbPos = new StringBuilder();
            StringBuilder sbControl = new StringBuilder();
            sbPos.Append("North: " + (jet.St.Position.x / MathHelper.ScaleDownFactor).ToString(format) + "\n");
            sbPos.Append("East: " + (jet.St.Position.y / MathHelper.ScaleDownFactor).ToString(format) + "\n");
            sbPos.Append("Altitude: " + (-jet.St.Position.z / MathHelper.ScaleDownFactor).ToString(format) + "\n");
            sbPos.Append("Velocity: " + (jet.St.Speed / MathHelper.ScaleDownFactor).ToString(format) + "\n");

            positionText = sbPos.ToString();

            sbControl.Append("Thrust: " + (jet.Control.Thrust / MathHelper.ScaleDownFactor).ToString(format) + "\n");
            sbControl.Append("Elevator: " + (-jet.Control.Elevator).ToString(format) + "\n");
            sbControl.Append("Aileron: " + jet.Control.Aileron.ToString(format) + "\n");
            sbControl.Append("Rudder: " + jet.Control.Rudder.ToString(format) + "\n");
            sbControl.Append("ThrustAngle: " + jet.Control.ThrustAngle.ToString(format) + "\n");
            float angleAlpha = Mathf.Rad2Deg * jet.St.Alpha;
            sbControl.Append("Alpha: " + angleAlpha.ToString(format) + "\n");
            sbControl.Append("Beta: " + (Mathf.Rad2Deg * jet.St.Beta).ToString(format) + "\n");

            controlText = sbControl.ToString();
        }

        private void SetInfoText()
        {
            string position, control;
            GetInfoText(out position, out control);
            positionText.text = position;
            controlText.text = control;
        }

        void OnServerUpdate(State newState)
        {
            serverState = newState;
            if (isLocalPlayer) jet.St = newState;
            if (stateHandler == null) return;
            stateHandler.OnStateChanged(serverState);
        }

        public double GetMachineTime()
        {
            return Multiplayer.MultiplayerManager.GetTime();
        }

        [Command]
        public void CmdMoveOnServer(AircraftControl input)
        {
            server.AddInput(input);
        }

        [Command]
        public void CmdRespawn()
        {
            serverState = State.CreateStartState();
        }

        void RespawnPlayer()
        {
            CmdRespawn();
            MessageBox.Instance.ButtonReturnMessageBoxClick();
            hasNoFlightError = true;
            wasPlayerAlreadyRespawned = false;
            areAnglesInLimits = true;
            if (userInput != null) userInput.OnRespawnPlayer();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!isLocalPlayer || !areAnglesInLimits) return;
            if (other.gameObject.name == "Water")
            {
                MessageBox.Instance.DisplayMessage("Water Collision", "Plane has crushed into water.\n\nRespawning in a moment", false);
                hasNoFlightError = false;
            }
        }
    }
}
