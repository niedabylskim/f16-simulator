﻿using Assets.Scripts.Physics;

namespace Assets.Scripts.Aircraft
{
    public interface IClientStateHandler
    {
        void OnStateChanged(State newState);
        State GetCurrentState();
    }
}
