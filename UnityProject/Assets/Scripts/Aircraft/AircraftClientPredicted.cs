﻿using Assets.Scripts.F16Model;
using Assets.Scripts.Physics;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Aircraft
{
    public class AircraftClientPredicted : MonoBehaviour, IClientStateHandler
    {
        private List<AircraftControl> pendingInputs;
        public AircraftClient player;
        public State predictedState;
        void Awake()
        {
            pendingInputs = new List<AircraftControl>();
            player = GetComponent<AircraftClient>();
            UpdatePredictedState();
        }

        public void OnStateChanged(State newState)
        {
            if (pendingInputs == null) return;            
            int i = 0;
            while (i < pendingInputs.Count)
            {
                if (pendingInputs[i].Info.MoveNumber <= newState.Info.MoveNumber)
                    pendingInputs.RemoveAt(i);
                else
                {
                    State.Move(newState, pendingInputs[i], player.jet, out newState);
                    i++;
                }
            }
            predictedState = newState;
        }

        private void UpdatePredictedState()
        {
            predictedState = player.serverState;
            foreach (var input in pendingInputs)
                State.Move(predictedState, input, player.jet, out predictedState);
        }


        public void AddInput(AircraftControl input)
        {
            if (!player.hasNoFlightError) return;
            var tmp = predictedState;
            bool isMoveCorrect = State.Move(predictedState, input, player.jet, out predictedState);         

            if (!isMoveCorrect || !checkIfAngleOfAttacksAreCorrect(predictedState.Alpha,predictedState.Beta))
            {
                MessageBox.Instance.DisplayMessage("Angle of attack", "Keep an eye on angles of attack. Alpha should be in range [-20;45] and beta in range [-30;30].\n\nRespawning in a moment", false);
                player.areAnglesInLimits = false;
                player.hasNoFlightError = false;
            }
            else
                pendingInputs.Add(input);
        }

        private bool checkIfAngleOfAttacksAreCorrect(float alpha, float beta)
        {
            float alphaDeg = predictedState.Alpha * Mathf.Rad2Deg;
            float betaDeg = predictedState.Beta * Mathf.Rad2Deg;
            float maxBeta = player.jet.Specification.MaxBetaDeg + MathHelper.AngleErrorEpsilon;
            float minBeta = player.jet.Specification.MinBetaDeg - MathHelper.AngleErrorEpsilon;
            float maxAlpha= player.jet.Specification.MaxAlphaDeg + MathHelper.AngleErrorEpsilon;
            float minAlpha = player.jet.Specification.MinAlphaDeg - MathHelper.AngleErrorEpsilon;
            return betaDeg <= maxBeta  && betaDeg >= minBeta && alphaDeg >= minBeta && alphaDeg <= maxAlpha;
        }

        public State GetCurrentState()
        {
            return predictedState;
        }
    }
}
