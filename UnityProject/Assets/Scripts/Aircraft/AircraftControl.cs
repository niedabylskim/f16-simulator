﻿using Assets.Scripts.Physics;

namespace Assets.Scripts.Aircraft
{
    public struct AircraftControl
    {
        public float Elevator;
        public float Aileron;
        public float Rudder;
        public float Lef;
        public float Thrust;
        public float ThrustAngle;
        public MoveInfo Info;

    }
}