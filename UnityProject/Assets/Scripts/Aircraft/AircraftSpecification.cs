﻿namespace Assets.Scripts.Aircraft
{
    public abstract class AircraftSpecification
    {
        protected float b, s, cbar, xcgr, xcg, maxAileronAngle, maxElevatorAngle, maxRudderAngle, maxThrustAngle, maxThrust, maxLefAngle, jy, jxz, jz, jx, maxAlphaDeg, maxBetaDeg, minAlphaDeg, minBetaDeg;
        public abstract float B { get; }        // span, m 
        public abstract float S { get; }              // planform area, m^2 
        public abstract float Cbar { get; }             // mean aero chord, m 
        public abstract float Xcgr { get; }      // reference center of gravity as a fraction of cbar 
        public abstract float Xcg { get; }         // center of gravity as a fraction of cbar
        public abstract float MaxAileronAngle { get; }
        public abstract float MaxElevatorAngle { get; }
        public abstract float MaxRudderAngle { get; }
        public abstract float MaxThrustAngle { get; }
        public abstract float MaxThrust { get; }
        public abstract float MaxLefAngle { get; }
        public abstract float Jy { get; }          // km-m^2
        public abstract float Jxz { get; }           // kg-m^2
        public abstract float Jz { get; }    // kg-m^2 
        public abstract float Jx { get; }         // kg-m^2

        public abstract float MaxAlphaDeg { get; } // deg
        public abstract float MaxBetaDeg { get; } // deg
        public abstract float MinAlphaDeg { get; }
        public abstract float MinBetaDeg { get; }

    }
}