﻿using Assets.Scripts.Physics;
using UnityEngine;

namespace Assets.Scripts.Aircraft
{
    public interface IAerodynamicData
    {
        bool GetAerodynamicForcesAndMoments(Parameters p, out Vector3 forces, out Vector3 moments);
    }
}