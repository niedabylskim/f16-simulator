﻿using Assets.Scripts.F16Model;
using Assets.Scripts.Missiles;
using Assets.Scripts.Physics;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Aircraft
{
    public class MultiplayerServer : MonoBehaviour
    {
        private Queue<AircraftControl> bufferOnServer;
        private const double sendRate = 10;
        private double lastSyncTime;

        AircraftClient player;
        Missile missile;

        void Awake()
        {
            bufferOnServer = new Queue<AircraftControl>();
            player = GetComponent<AircraftClient>();
            var tmp = State.CreateStartState();
            player.serverState = tmp;
            lastSyncTime = Scripts.Multiplayer.MultiplayerManager.GetTime();
        }

        void FixedUpdate()
        {
            UpdateServer();
        }

        void UpdateServer()
        {
            double tmpt = Network.time;
            if (tmpt - lastSyncTime > 1 / sendRate)
            {
                lastSyncTime = tmpt;
                MoveAutoritative();
            }
        }

        private void MoveAutoritative()
        {
            State tmp = player.serverState;
            while (bufferOnServer.Count != 0)
            {
                AircraftControl q = bufferOnServer.Dequeue();
                State.Move(tmp, q, player.jet, out tmp);                
            }
            player.serverState = tmp;
        }

        public void AddInput(AircraftControl input)
        {
            bufferOnServer.Enqueue(input);
        }
    }
}
