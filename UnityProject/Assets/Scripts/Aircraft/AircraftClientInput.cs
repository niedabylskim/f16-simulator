﻿using Assets.Scripts.F16Model;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Aircraft
{
    public class AircraftClientInput : MonoBehaviour
    {
        List<AircraftControl> inputBuffer;
        AircraftClient player;
        AircraftClientPredicted predicted;
        bool isControlTrimmed = true;

        private const float thrustChange = 1000 * MathHelper.ScaleDownFactor;
        private const float steeringChange = 0.1f;

        void Awake()
        {
            inputBuffer = new List<AircraftControl>();
            player = GetComponent<AircraftClient>();
            predicted = GetComponent<AircraftClientPredicted>();
        }

        void FixedUpdate()
        {
            if (!player.hasNoFlightError) return;
            if (Input.GetButtonDown("Cancel"))
            {
                SettingsManager.Instance.StartPlayingSong();
                if (player.isServer) NetworkManager.singleton.StopServer();
                if (player.isLocalPlayer) NetworkManager.singleton.StopClient();
            }
            GetUserInput();
        }

        private AircraftControl GetTrimmedControl(float elevator, float aileron, float ruddder, float thrust, float thrustAngle)
        {
            var trimmedElevatorDelta = elevator > 0.1f ? steeringChange : elevator < -0.1f ? -steeringChange : 0.0f;
            var trimmedAileronDelta = aileron > 0.1f ? -steeringChange : aileron < -0.1f ? steeringChange : 0.0f;
            var trimmedRudderDelta = ruddder > 0.1f ? steeringChange : ruddder < -0.1f ? -steeringChange : 0.0f;
            var trimmedThrustAngle = thrustAngle > 0.1f ? steeringChange : thrustAngle < -0.1f ? -steeringChange : 0.0f;
            var control = new AircraftControl
            {
                Elevator = Mathf.Clamp(player.jet.Control.Elevator + trimmedElevatorDelta, -player.jet.Specification.MaxElevatorAngle, player.jet.Specification.MaxElevatorAngle),
                Aileron = Mathf.Clamp(player.jet.Control.Aileron + trimmedAileronDelta, -player.jet.Specification.MaxAileronAngle, player.jet.Specification.MaxAileronAngle),
                Rudder = Mathf.Clamp(player.jet.Control.Rudder + trimmedRudderDelta, -player.jet.Specification.MaxRudderAngle, player.jet.Specification.MaxRudderAngle),
                ThrustAngle = Mathf.Clamp(player.jet.Control.ThrustAngle + trimmedThrustAngle, -player.jet.Specification.MaxThrustAngle, player.jet.Specification.MaxThrustAngle),
                Thrust = Mathf.Clamp(thrust, 0, player.jet.Specification.MaxThrust),
                Lef = player.jet.Control.Lef,
                Info = new Scripts.Physics.MoveInfo(player.GetMachineTime())
            };
            return control;
        }
        private AircraftControl GetChangingControl(float elevator, float aileron, float ruddder, float thrust, float thrustAngle)
        {
            float interpClamp = 0.3f;
            float elevInterp = MathHelper.GetInterCoef(player.jet.Control.Elevator, player.jet.Specification.MaxElevatorAngle, interpClamp);
            float ailInterp = MathHelper.GetInterCoef(player.jet.Control.Aileron, player.jet.Specification.MaxAileronAngle, interpClamp);
            float rudInterp = MathHelper.GetInterCoef(player.jet.Control.Rudder, player.jet.Specification.MaxRudderAngle, interpClamp);
            float thrInterp = MathHelper.GetInterCoef(player.jet.Control.ThrustAngle, player.jet.Specification.MaxThrustAngle, interpClamp);
            var control = new AircraftControl
            {
                Elevator = Mathf.Lerp(player.jet.Control.Elevator, elevator * player.jet.Specification.MaxElevatorAngle, elevInterp),
                Aileron = Mathf.Lerp(player.jet.Control.Aileron, -aileron * player.jet.Specification.MaxAileronAngle, ailInterp),
                Rudder = Mathf.Lerp(player.jet.Control.Rudder, ruddder * player.jet.Specification.MaxRudderAngle, rudInterp),
                ThrustAngle = Mathf.Lerp(player.jet.Control.ThrustAngle, thrustAngle * player.jet.Specification.MaxThrustAngle, thrInterp),
                Thrust = Mathf.Clamp(thrust, 0, player.jet.Specification.MaxThrust),
                Lef = player.jet.Control.Lef,
                Info = new Scripts.Physics.MoveInfo(player.GetMachineTime())
            };
            return control;
        }
        private void GetUserInput()
        {
            var elevAxis = Input.GetAxis("ElevatorAxis");
            var ailAxis = Input.GetAxis("AileronAxis");
            var rudAxis = Input.GetAxis("RudderAxis");
            var thrustAngleAxis = Input.GetAxis("ThrustAngleAxis");
            float thrust = player.jet.Control.Thrust;
            if (Input.GetButton("LeftBumper")) thrust -= thrustChange;
            if (Input.GetButton("RightBumper")) thrust += thrustChange;
            if (Input.GetButtonDown("TrimControl")) isControlTrimmed = !isControlTrimmed;
            player.jet.Control = isControlTrimmed ?
                GetTrimmedControl(elevAxis, ailAxis, rudAxis, thrust, thrustAngleAxis) :
                GetChangingControl(elevAxis, ailAxis, rudAxis, thrust, thrustAngleAxis);
            float thrustDelta = player.jet.Specification.MaxThrust - player.jet.Control.Thrust;
            if (thrustDelta == 0 && !player.ThrustFlame.isPlaying) player.ThrustFlame.Play();
            else if (thrustDelta != 0) player.ThrustFlame.Stop();
            predicted.AddInput(player.jet.Control);
            player.CmdMoveOnServer(player.jet.Control);
        }
        public void OnRespawnPlayer()
        {
            isControlTrimmed = false;
        }
    }
}
