﻿using Assets.Scripts.Physics;
namespace Assets.Scripts.Aircraft
{
    public class AircraftParameters : Parameters
    {
        public AircraftControl Control;
        public AircraftSpecification Specification { get; set; }
        protected override Parameters Add(State b)
        {
            return new AircraftParameters { St = St + b, Control = Control, Specification = Specification };
        }
        protected override Parameters Divide(float f)
        {
            return new AircraftParameters { St = St * f, Control = Control, Specification = Specification };
        }
        protected override Parameters Multiply(float f)
        {
            return new AircraftParameters { St = St / f, Control = Control, Specification = Specification };
        }
    }
}