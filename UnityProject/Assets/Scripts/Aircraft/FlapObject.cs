﻿using UnityEngine;

namespace Assets.Scripts.Aircraft
{
    public class FlapObject
    {
        public Quaternion Rotation
        {
            get { return flap.transform.rotation; }
            set { flap.transform.rotation = value; }
        }
        public Quaternion StartRotation { get; private set; }
        private GameObject flap;

        public FlapObject(GameObject _flap)
        {
            flap = _flap;
            StartRotation = _flap.transform.rotation;
        }
    }
}