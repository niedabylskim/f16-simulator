﻿using Assets.Scripts.Physics;
using UnityEngine;

namespace Assets.Scripts.Aircraft
{
    public abstract class Aircraft : RigidBody
    {
        protected IAerodynamicData Data { get; set; }
        public AircraftSpecification Specification { get; protected set; }
        public AircraftControl Control;
        public Aircraft() { }
        public Aircraft(Vector3 translation, Quaternion rotation, float velocity = 0, Vector3? angularVelocity = null) : base(translation, rotation, velocity, angularVelocity)
        { }
    }
}