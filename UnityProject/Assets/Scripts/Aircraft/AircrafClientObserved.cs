﻿using Assets.Scripts.F16Model;
using Assets.Scripts.Physics;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Aircraft
{
    public class AircraftUserObserved : MonoBehaviour, IClientStateHandler
    {
        State interpolated;
        public AircraftClient player;
        LinkedList<State> interpolationBuffer;

        public Vector3 LastKnownState;
        private double sendRatio = 0.1; //server sendrate

        void Awake()
        {
            player = GetComponent<AircraftClient>();
            interpolationBuffer = new LinkedList<State>();
            SetObservedState(player.serverState);
            AddState(player.serverState);
        }

        void FixedUpdate()
        {
            InterpolateEntities();
        }

        public void OnStateChanged(State newState)
        {
            AddState(newState);
        }
        void SetObservedState(State newState)
        {
            interpolated = newState;
            //player.SyncState(interpolated);
        }

        void AddState(State state)
        {
            interpolationBuffer.AddLast(state);
            LastKnownState = state.Position;
        }

        private void InterpolateEntities()
        {
            var renderTimestamp = player.GetMachineTime() - sendRatio;
            while (interpolationBuffer.Count >= 2 && interpolationBuffer.First.Next.Value.Info.Timestamp <= renderTimestamp)
            {
                interpolationBuffer.RemoveFirst();
            }
            if (interpolationBuffer.Count >= 2
                && interpolationBuffer.First.Value.Info.Timestamp <= renderTimestamp
                && renderTimestamp <= interpolationBuffer.First.Next.Value.Info.Timestamp)
            {
                var t0 = interpolationBuffer.First.Value.Info.Timestamp;
                var t1 = interpolationBuffer.First.Next.Value.Info.Timestamp;
                var x0 = interpolationBuffer.First.Value.Position;
                var x1 = interpolationBuffer.First.Next.Value.Position;
                var q0 = interpolationBuffer.First.Value.Orientation;
                var q1 = interpolationBuffer.First.Next.Value.Orientation;
                float coeff = (float)((renderTimestamp - t0) / (t1 - t0));
                interpolated.Position = Vector3.Lerp(x0, x1, coeff);
                interpolated.Orientation = Quaternion.Slerp(q0, q1, coeff);
            }
            if (interpolationBuffer.Count == 1 && interpolationBuffer.First.Value.Info.Timestamp <= renderTimestamp)
            {
                interpolated.Position = interpolationBuffer.First.Value.Position;
                interpolated.Orientation = interpolationBuffer.First.Value.Orientation;
            }
            SetObservedState(interpolated);
        }

        public State GetCurrentState()
        {
            return interpolated;
        }
    }
}
