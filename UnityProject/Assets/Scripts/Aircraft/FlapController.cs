﻿using UnityEngine;

namespace Assets.Scripts.Aircraft
{
    public class FlapController
    {
        FlapObject AileronLeft;
        FlapObject AileronRight;
        FlapObject ElevatorLeft;
        FlapObject ElevatorRight;
        FlapObject LefLeft;
        FlapObject LefRight;
        FlapObject Rudder;
        //public Quaternion ParentRotation { get; set; }

        public FlapController(params GameObject[] par)
        {
            Rudder = new FlapObject(par[0]);
            AileronLeft = new FlapObject(par[1]);
            AileronRight = new FlapObject(par[2]);
            ElevatorLeft = new FlapObject(par[3]);
            ElevatorRight = new FlapObject(par[4]);
            LefLeft = new FlapObject(par[5]);
            LefRight = new FlapObject(par[6]);
        }

        public void Rotate(AircraftControl ac, Quaternion parentRotation)
        {
            Rudder.Rotation = parentRotation * Rudder.StartRotation * Quaternion.Euler(0, ac.Rudder, 0);
            AileronLeft.Rotation = parentRotation * AileronLeft.StartRotation * Quaternion.Euler(0, 0, ac.Aileron);
            AileronRight.Rotation = parentRotation * AileronRight.StartRotation * Quaternion.Euler(0, 0, -ac.Aileron);
            ElevatorLeft.Rotation = parentRotation * ElevatorLeft.StartRotation * Quaternion.Euler(0, 0, ac.Elevator);
            ElevatorRight.Rotation = parentRotation * ElevatorRight.StartRotation * Quaternion.Euler(0, 0, ac.Elevator);
            LefLeft.Rotation = parentRotation * LefLeft.StartRotation * Quaternion.Euler(0, 0, -ac.Lef);
            LefRight.Rotation = parentRotation * LefRight.StartRotation * Quaternion.Euler(0, 0, -ac.Lef);
        }
    }
}