﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class MenuFunctions : MonoBehaviour {
    
    public GameObject panelPlay;
    public GameObject panelExit;
    public Dropdown dropdownFullScreen;

    public Slider SfxSoundSlider;
    public Slider MusicSoundSlider;
    public Toggle Hifi, Lofi;
    public Image buttonGameHighlight, buttonControllsHighlight, buttonVideoHighlight;
    public GameObject panelGame, panelControlls, panelVideo;    
    public InputField IpField;
    public Text buttonText;
    private Animator animator;

    public Button playHost, playClient;

    private bool ipHasError = false;
    bool isStarting = false;
    private string currentNameScene;
    void Awake()
    {
        Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
    }
    void Start () {
        currentNameScene = SceneManager.GetActiveScene().name;
        Screen.fullScreen = SettingsManager.Instance.IsFullScreen;
        if (SettingsManager.Instance.IsHifiModel) Hifi.isOn = true;
        else Lofi.isOn = true;
        SfxSoundSlider.value = SettingsManager.Instance.SfxVolume;
        MusicSoundSlider.value = SettingsManager.Instance.MusicVolume;
        if (string.IsNullOrEmpty(SettingsManager.Instance.Ip))
            SettingsManager.Instance.SetIp(IpField.text);
        IpField.text = SettingsManager.Instance.Ip;
        buttonText.text = SettingsManager.Instance.IsFullScreen ? "On" : "Off";
        animator = GetComponent<Animator>();
        SetResolutionDropdown();
        ChangeResolution(SettingsManager.Instance.CurrentResolutionIndex);
        QualitySettings.SetQualityLevel(5);
        SettingsManager.Instance.OnChangeIsConnecting += Instance_OnChangeIsConnecting;
    }

    private void Instance_OnChangeIsConnecting(bool value)
    {
        Debug.Log(value);
        if (!value && SceneManager.GetActiveScene().name == currentNameScene)
            MessageBox.Instance.DisplayMessage("Timeout!", "Couldnt connect to remote host.\nPlease check if IP address is correct.");
        ChangeInteractibility(!value);
    }

    public void ValidateUserInput(String input)
    {
        if (EventSystem.current.currentSelectedGameObject == null) return;
        var obj = EventSystem.current.currentSelectedGameObject.GetComponent<Image>();
        if (ValidateIPv4(input))
        {
            ipHasError = false;
            obj.color = new Color(1, 0, 0, 0.0f);
            SettingsManager.Instance.SetIp(input);
        }
        else
        {
            ipHasError = true;
            obj.color = new Color(1.0f, 0, 0, 0.2f);
        }
    }
    private bool ValidateIPv4(string ipString)
    {
        if (string.IsNullOrEmpty(ipString)) return false;
        string[] splitValues = ipString.Split('.');
        if (splitValues.Length != 4) return false;
        byte tempForParsing;
        return splitValues.All(r => byte.TryParse(r, out tempForParsing));
    }
    private void SetResolutionDropdown()
    {
        dropdownFullScreen.options.Clear();
        //it fixes bug after building from editor to stand-alone version
        var names = SettingsManager.Instance.AvaiableResolutions.Distinct().ToList();
        for (int i = 0; i < names.Count; i++)
        {
            dropdownFullScreen.options.Add(new Dropdown.OptionData(names[i]));
            dropdownFullScreen.options[i].text = names[i];
        }
        if(dropdownFullScreen.value == SettingsManager.Instance.CurrentResolutionIndex)
        {
            if (dropdownFullScreen.value == 0)
            {
                isStarting = true;
                dropdownFullScreen.value = 1;
                dropdownFullScreen.value = SettingsManager.Instance.CurrentResolutionIndex;
                isStarting = false;
            }
            ChangeResolution(dropdownFullScreen.value);
            return;
        }
        dropdownFullScreen.value = SettingsManager.Instance.CurrentResolutionIndex;
    }

    public void ChangeResolution(int i)
    {
        if (isStarting) return;
        SettingsManager.Instance.SetResolutionIndex(i);
        var text = dropdownFullScreen.options[i].text.Split(' ');
        var width = text[0];
        var height = text[2];
        Screen.SetResolution(int.Parse(width), int.Parse(height), SettingsManager.Instance.IsFullScreen);
    }

    public void ShowPlayPanel()
    {
        panelExit.SetActive(false);
        panelPlay.SetActive(true);
    }
    public void ShowExitPanel()
    {
        panelExit.SetActive(true);
        panelPlay.SetActive(false);
    }
    public void ButtonSettingsClick(int i)
    {
        switch(i)
        {
            case 0:
                panelControlls.SetActive(false);
                buttonControllsHighlight.gameObject.SetActive(false);
                panelVideo.SetActive(false);
                buttonVideoHighlight.gameObject.SetActive(false);
                panelGame.SetActive(true);
                buttonGameHighlight.gameObject.SetActive(true);
                break;
            case 1:
                panelVideo.SetActive(false);
                buttonVideoHighlight.gameObject.SetActive(false);
                panelGame.SetActive(false);
                buttonGameHighlight.gameObject.SetActive(false);
                panelControlls.SetActive(true);
                buttonControllsHighlight.gameObject.SetActive(true);
                break;
            case 2:
                panelGame.SetActive(false);
                buttonGameHighlight.gameObject.SetActive(false);
                panelControlls.SetActive(false);
                buttonControllsHighlight.gameObject.SetActive(false);
                panelVideo.SetActive(true);
                buttonVideoHighlight.gameObject.SetActive(true);
                break;
        }
    }
    public void ButtonChangeOnOffTextClick()
    {
        var button = EventSystem.current.currentSelectedGameObject;
        var text = button.GetComponentInChildren<Text>();
        text.text = text.text == "On" ? "Off" : "On";
    }
    public void SwitchFullScreen()
    {
        SettingsManager.Instance.SetIsFullScreen(!SettingsManager.Instance.IsFullScreen);
        Screen.fullScreen = SettingsManager.Instance.IsFullScreen;
    }

    public void SwitchPhysicalModel(bool value)
    {
        SettingsManager.Instance.SetIsHifiModel(value);
    }
    public void ButtonPlayAsHostClick()
    {
        SettingsManager.Instance.StopPlayingSong();
        NetworkManager.singleton.StartHost();
    }
    public void ButtonMainPanelSettingsClick()
    {
        animator.SetInteger("AnimationNumber", 1);
        panelPlay.SetActive(false);
        panelExit.SetActive(false);
    }
    public void ButtonMainPanelAboutClick()
    {
        animator.SetInteger("AnimationNumber", 3);
        panelPlay.SetActive(false);
        panelExit.SetActive(false);
    }
    public void ButtonAboutReturnClick()
    {
        animator.SetInteger("AnimationNumber", 4);
        EventSystem.current.SetSelectedGameObject(EventSystem.current.firstSelectedGameObject);
    }
    public void ButtonSettingsReturnClick()
    {
        animator.SetInteger("AnimationNumber", 2);
        EventSystem.current.SetSelectedGameObject(EventSystem.current.firstSelectedGameObject);
    }
    public void ButtonPlayAsClientClick()
    {
        if (ipHasError) return;
        NetworkManager.singleton.networkAddress = SettingsManager.Instance.Ip;
        var client = NetworkManager.singleton.StartClient();
        SettingsManager.Instance.ChangeIsConnecting(true);
    }

    public void ButtonExitNoClick()
    {
        panelExit.SetActive(false);
        EventSystem.current.SetSelectedGameObject(EventSystem.current.firstSelectedGameObject);
    }
    public void ButtonExitYesClick()
    {
        Application.Quit();
    }

    public void ChangeSoundVolume(float s)
    {
        SettingsManager.Instance.ChangeSoundVolume(s);
    }
    public void ChangeSfxVolume(float s)
    {
        SettingsManager.Instance.ChangeSfxVolume(s);
    }

    public void ChangeInteractibility(bool value)
    {
        if (SceneManager.GetActiveScene().name != currentNameScene) return;
        var buttonH = GameObject.Find("Button_PlayHost").GetComponent<Button>();
        var buttonC = GameObject.Find("Button_PlayClient").GetComponent<Button>();
        var input = GameObject.Find("InputField").GetComponent<InputField>();
        if (buttonH != null) buttonH.interactable = value;
        if (buttonC != null) buttonC.interactable = value;
        if (input != null) input.interactable = value;
    }

}
