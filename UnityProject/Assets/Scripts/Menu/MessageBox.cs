﻿using UnityEngine;
using UnityEngine.UI;

public sealed class MessageBox : MonoBehaviour
{

    private static MessageBox _instance;

    private bool isButtonActive = true;

    public static MessageBox Instance
    {
        get
        {
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
            Instance.gameObject.SetActive(false);
        }
    }

    // Use this for initialization
    public void DisplayMessage(string title, string message, bool showButton = true)
    {
        var canvas = GetComponent<Canvas>();
        Text[] texts = canvas.GetComponentsInChildren<Text>();
        texts[0].text = message;
        texts[1].text = title;

        var button = canvas.GetComponentInChildren<Button>(true);
        isButtonActive = showButton;
        if (!showButton) button.gameObject.SetActive(isButtonActive);

        Instance.gameObject.SetActive(true);
    }

    public void DisplayMessageAndHideAfterTime(string title, string message, bool showButton = true, float time = 1)
    {
        var canvas = GetComponent<Canvas>();
        Text[] texts = canvas.GetComponentsInChildren<Text>();
        texts[0].text = message;
        texts[1].text = title;

        var button = canvas.GetComponentInChildren<Button>(true);
        isButtonActive = showButton;
        if (!showButton) button.gameObject.SetActive(isButtonActive);

        Instance.gameObject.SetActive(true);

        Invoke("ButtonReturnMessageBoxClick", time);

    }

    public void ButtonReturnMessageBoxClick()
    {
        Instance.gameObject.SetActive(false);
    }
}
