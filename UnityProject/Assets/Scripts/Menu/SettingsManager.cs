﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public sealed class SettingsManager : MonoBehaviour {

    
    public AudioClip[] soundtracks;
    private AudioSource source;
    private int index;
    public float SfxVolume { get; private set; }
    public float MusicVolume { get; private set; }
    public bool IsHifiModel { get; private set; }
    public bool IsFullScreen { get; private set; }
    public int CurrentResolutionIndex { get; private set; }
    public List<string> AvaiableResolutions { get; private set; }

    public string Ip { get; private set; }
    public bool IsConnecting { get; private set; }

    public delegate void ChangeIsConnectingAction(bool value);
    public event ChangeIsConnectingAction OnChangeIsConnecting;

    private static SettingsManager _instance;

    public static SettingsManager Instance
    {
        get
        {
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance != null && _instance != this )
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
            OnInitialize();
        }
    }

    private void OnInitialize()
    {
        SfxVolume = 1;
        Ip = string.Empty;
        MusicVolume = 1;
        IsHifiModel = true;
        IsFullScreen = true;
        GetSupportedScreenResolutions();
        StartPlayingSong();
    }

    private void GetSupportedScreenResolutions()
    {
        AvaiableResolutions = new List<string>();
        var resoulutions = Screen.resolutions.Where(s => s.width > 1024 && s.height >= 768).Distinct().ToArray();
        var currRes = Screen.currentResolution;
        int index = 0;
        for (int i = 0; i < resoulutions.Length; i++)
        {
            string name = resoulutions[i].width + " x " + resoulutions[i].height;
            if (resoulutions[i].width == currRes.width && resoulutions[i].height == currRes.height) index = i;
            AvaiableResolutions.Add(name);
        }
        CurrentResolutionIndex = index;
    }

    public void StartPlayingSong()
    {
        index = UnityEngine.Random.Range(0, soundtracks.Length);
        source = GetComponent<AudioSource>();
        source.clip = soundtracks[index];
        source.loop = false;
        source.Play();
        Invoke("PlayNextSong", source.clip.length);
    }

    private void PlayNextSong()
    {
        index++;
        if (index >= soundtracks.Length) index = 0;
        source.clip = soundtracks[index];
        source.Play();
        Invoke("PlayNextSong", source.clip.length);
    }

    public void ChangeSoundVolume(float s)
    {
        MusicVolume = s;
        source.volume = s;
    }
    public void ChangeSfxVolume(float s)
    {
        SfxVolume = s;
    }
    public void StopPlayingSong()
    {
        source.Stop();
        CancelInvoke("PlayNextSong");
    }

    public void SetIsHifiModel(bool value)
    {
        IsHifiModel = value;
    }
    public void SetIsFullScreen(bool value)
    {
        IsFullScreen = value;
    }
    public void SetResolutionIndex(int i)
    {
        CurrentResolutionIndex = i;
    }
    public void ChangeIsConnecting(bool value)
    {
        IsConnecting = value;
        OnChangeIsConnecting(value);
    }
    public void SetIp(string value)
    {
        Ip = value;
    }
}
